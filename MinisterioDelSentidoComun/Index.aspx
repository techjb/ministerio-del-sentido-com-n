﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="EspañisBook.Index" %>
<html>
<head>
	<title>Cómo deberían ser las gestiones con la administración pública.</title>	
	<meta http-equiv="content-type" content="text/html;charset=UTF-8" />    
	<meta name="keywords" content="administración pública, opendata, ministerio del sentido común" />
	<meta name="description" content="Una idea de cómo debería de ser la administración pública." />
	<meta http-equiv="imagetoolbar" content="no" />
	<link rel="shortcut icon" href="img/favicon.ico?v=1.01" type="image/x-icon" />
	<link rel="stylesheet" href="css/easy.css" media="screen" />
	<!--link rel="stylesheet" href="css/easyprint.css" media="print" /-->
	<!--script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js"></script-->
	<script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" src="js/jquery.qtip-1.0.0-rc3.min.js"></script>
    <script type="text/javascript" src="js/easy.js"></script>
	<script type="text/javascript" src="js/main.js"></script>    
	<script type="text/javascript" src="js/fancybox/jquery.fancybox.js?v=2.0.6"></script>
	<link rel="stylesheet" type="text/css" href="js/fancybox/jquery.fancybox.css?v=2.0.6" media="screen" />
    <script type="text/javascript" src="js/date-es-ES.js"></script>
    <script type="text/javascript" src="js/time.js"></script>

    <script type="text/javascript">
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-386914-30']);
        _gaq.push(['_trackPageview']);
        (function () {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
    </script>
    
    
</head>
<body>

<div id="container">

	<div id="header">
		<div id="logo">
            <img alt="logo" src="img/logoMinisterio.png" />
        </div>
        <div>
		    <form id="searchForm" action="/" method="post">					
	            <label for="keyword">Buscar</label>
	            <input type="text" name="keyword" id="keyword" size="30" class="field label" />
	            <button type="submit" class="graphic">Go</button>		
	
            </form>	
		</div>
        <div id="userInfo">
            <!--iframe src="https://www.facebook.com/plugins/like.php?href=http://www.ministeriodelsentidocomun.com" scrolling="no" frameborder="0" style="border:none; width:350px; height:80px;position:absolute;right:260px;margin-top:8px;border:0px solid red;"></iframe-->
            <a  id="link-datospersonales" href="#datospersonales" >Antonio García Rubio<img alt="Datos identificativos" src="img/fotocarnet.jpg" /></a>
            <a href="javascript:EsSoloUnaIdea();" style="margin-left:20px;text-decoration:underline;"  title="Cerrar la aplicación">Salir</a>

        </div>
	</div>

	<div class="contentMiddle">

		<div class="main">
            <ul class="list">
	            <li><a id="link-notificaciones" href="#notificaciones" style="margin-bottom:20px;"><img  alt="Notificaciones" src="img/mail.png" />Notificaciones (2)</a></li>
                <li><a id="link-seguridadsocial" href="#seguridadsocial"><img  alt="Seguridad social" src="img/seguridadsocial.png" />Seguridad social</a></li>
                <li><a id="link-trafico" href="#trafico"><img  alt="Tráfico" src="img/trafico.png" />Tráfico</a></li>
	            <li><a id="link-educacion" href="#educacion"><img  alt="Educación" src="img/educacion.png" />Educación</a></li>
                <!--li><a id="link-trabajo" href="#trabajo"><img  alt="Trabajo" src="img/trabajo.png" />Trabajo</a></li-->
                <li><a id="link-justicia" href="#justicia"><img  alt="Justicia" src="img/justicia.png" />Justicia</a></li>
	            <li><a id="link-impuestos" href="#impuestos"><img  alt="Impuestos" src="img/impuestos.png" />Impuestos</a></li>
	           
            </ul>
        </div>		
		<div class="secondary">
            <div id="trafico" class="panel">
			    <div class="content">
				    <h3>Tráfico</h3>
				    <ul class="tabs tabs2 fixed">
                        <li><a class="tabLeft" href="#tab11">Puntos</a></li>                        
                        <li><a class="tabCenter" href="#tab13">Multas (2)</a></li>
                        <li><a class="tabCenter" href="#tab14">Requerimientos (1)</a></li>
                        <li><a class="tabCenter" href="#tab15">Vehículos</a></li>                    
                        <li><a class="tabRight" href="#tab12">Permisos</a></li>                        
                   </ul>
                   <div class="tab" id="tab11">
                        <div class="tabcontentwhite">
                         
                         <h4>Actualmente tienes <span style="font-weight:bold;color:Green;font-size:20px;">8</span> puntos.</h4>
                         
                         <table cellpadding="0" cellspacing="0" border="0" style="width:700px;">
                            <caption>Histórico de puntos</caption>
                            <thead><tr><th>Fecha</th><th>Movimiento</th><th>Puntos</th><th>Saldo final</th><th>Organismo</th></tr></thead>
                            <tbody>
                                <tr><td>18/04/2011</td><td>Sanción de tráfico</td><td align="center"><span style="font-weight:bold;color:Red;">-2</span></td><td  align="center">8</td><td>Jefatura Provincial De Madrid</td></tr>
                                <tr><td>25/10/2010</td><td>Sanción de tráfico</td><td align="center"><span style="font-weight:bold;color:Red;">-3</span></td><td  align="center">10</td><td>Jefatura Provincial De Madrid</td></tr>
                                <tr><td>04/02/2010</td><td>Sanción de tráfico</td><td align="center"><span style="font-weight:bold;color:Red;">-2</span></td><td  align="center">13</td><td>Jefatura Provincial De Madrid</td></tr>
                                <tr><td>04/02/2008</td><td>Bonificación</td><td align="center"><span style="font-weight:bold;color:Green;">+1</span></td><td align="center">15</td><td></td></tr>
                                <tr><td>04/02/2007</td><td>Bonificación</td><td align="center"><span style="font-weight:bold;color:Green;">+2</span></td><td align="center">14</td><td></td></tr>
                                <tr><td>01/05/2005</td><td>12 puntos iniciales</td><td align="center"><span style="font-weight:bold;color:Green;">12</span></td><td align="center">12</td><td></td></tr>
                                
                            </tbody>
                        </table>
                    </div>
                        <div class="tabBottom"></div>
                    </div>
                    <div class="tab" id="tab12">
                        <div class="tabcontentwhite">
                             <table cellpadding="0" cellspacing="0" border="0" style="width:500px;">
                                <caption>Permisos de circulación</caption>
                                <thead><tr><th></th><th>Válido desde</th><th>Válido hasta</th><th>Observaciones</th></tr></thead>
                                <tbody>
                                    <tr><td>A1<img alt="moto" src="img/motorcycle.png" style="vertical-align:middle;margin-left:8px;" /></td><td>16/07/2005</td><td>16/07/2025</td><td>(1)</td></tr>  
                                    <tr><td>A2<img alt="moto" src="img/motorbike.png" style="vertical-align:middle;margin-left:8px;" /></td><td>16/07/2005</td><td>16/07/2025</td><td>(1)</td></tr>  
                                    <tr><td>A<img alt="moto" src="img/motorbike.png" style="vertical-align:middle;margin-left:16px;" /></td><td>16/07/2005</td><td>16/07/2025</td><td>(1)</td></tr>  
                                    <tr><td>B<img alt="coche" src="img/car.png" style="vertical-align:middle;margin-left:16px;" /></td><td>1/09/2007</td><td>1/09/2027</td><td>(1)</td></tr>
                                </tbody>
                            </table>
                           <p>
                                <b>1.</b> Necesitas llevar lentes para conducir.
                            </p>
                            <p>
                                ¿Has perdido tu carnet? <a href="javascript:PedirCarnetNuevo();">Pedir un carnet nuevo</a>.<br />
                                ¿Quieres presentarte a un examen? <a href="javascript:EsSoloUnaIdea();">Inscribirme para examinarme</a>.
                            </p>
                        </div>
                        <div class="tabBottom"></div>
                    </div>

                    <div class="tab" id="tab13">
                        <div class="tabcontentwhite">
                           <table cellpadding="0" cellspacing="0" border="0" style="width:720px;table-layout:fixed">
                                <caption style="color:Red;">Pendientes de pago:</caption>
                                <thead><tr><th style="width:100px;">Fecha</th><th style="width:30px;">Lugar</th><th>Multado por</th><th style="width:60px;">Sanción</th><th style="width:40px;">Puntos</th><th nowrap="nowrap" style="width:100px;"><a href="javascript:PagarMulta(320, true);" >Pagar todas ></a></th></tr></thead>
                                <tbody>
                                    <tr><td  valign="top"><span id="date01"></span> 21:35</td><td valign="top"><a class="fancybox  various fancybox.iframe" href="https://maps.google.es/maps?f=q&amp;source=s_q&amp;hl=es&amp;geocode=&amp;q=C%2F+Gran+V%C3%ADa+23,+28028,+Madrid&amp;aq=&amp;sll=40.380028,-3.669434&amp;sspn=9.25215,21.643066&amp;t=w&amp;ie=UTF8&amp;hq=&amp;hnear=Calle+Gran+V%C3%ADa,+23,+28013+Madrid,+Comunidad+de+Madrid&amp;ll=40.419995,-3.701881&amp;spn=0.001129,0.002642&amp;z=19&amp;output=embed"><img src="img/marker.png"  alt="mapa" /></a></td><td valign="top">Estacionamiento en zona prohibida</td><td  valign="top">200&nbsp;€&nbsp;&nbsp;<img id="infoMulta1" title="" alt="" src="img/info.png" style="vertical-align:text-bottom"/></td><td valign="top">-2</td><td valign="top"><a href="javascript:RecurrirMulta();" >Recurrir</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:PagarMulta(200);" >Pagar</a></td></tr>
                                    <tr><td  valign="top"><span id="date02"></span> 09:40</td><td valign="top"><a class="fancybox  various fancybox.iframe" href="https://maps.google.es/maps?f=q&amp;source=s_q&amp;hl=es&amp;geocode=&amp;q=40.484361,-3.861574&amp;aq=&amp;sll=40.472917,-3.825794&amp;sspn=0.009027,0.021136&amp;t=w&amp;ie=UTF8&amp;z=18&amp;ll=40.484361,-3.861574&amp;output=embed"><img src="img/marker.png"  alt="mapa" /></a></td><td valign="top">Exceso de velocidad</td><td valign="top">120&nbsp;€&nbsp;&nbsp;<img title="100 € +  20% recargo = 120 €" alt="" src="img/info.png"  style="vertical-align:text-bottom" /></td><td valign="top"></td><td valign="top"><a href="javascript:RecurrirMulta();" >Recurrir</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:PagarMulta(120);" >Pagar</a></td></tr>
                                </tbody>
                            </table>

                            <a onclick="HistoricoDeMultas();" href="#tablaHistorialDeMultas" title="Mostrar el historial de multas" class="toggle id tooltip" ><img src="img/plus.png" style="vertical-align:text-bottom;margin-right:6px;" id="historicoPlus" alt="" />Histórico de multas</a>
                            <div id="tablaHistorialDeMultas" style="display:none;margin-top:10px;">
                                <table cellpadding="0" cellspacing="0" border="0" style="width:720px;table-layout:fixed;">
                                    <caption>En total has perdido 7 puntos y 1.250 €</caption>
                                    <thead>
                                        <tr><th style="width:100px;">Fecha</th><th style="width:30px;">Lugar</th><th>Multado por</th><th style="width:60px;">Sanción</th><th style="width:40px;">Puntos</th><th nowrap="nowrap" style="width:100px;"></th></tr>
                                    </thead>
                                    <tbody>
                                        <tr><td  valign="top">18/04/2011 04:12</td><td valign="top"><a class="fancybox  various fancybox.iframe" href="https://maps.google.es/maps?f=q&amp;source=s_q&amp;hl=es&amp;geocode=&amp;q=40.513673,-3.882175&amp;aq=&amp;&amp;t=w&amp;ie=UTF8&amp;z=18&amp;output=embed"><img src="img/marker.png"  alt="mapa" /></a></td><td valign="top">Exceso de velocidad</td><td  valign="top">300 €</td><td valign="top">-2</td><td valign="top"><img src="img/ok.png" alt="Multa pagada" /> Pagada</td></tr>
                                        <tr><td  valign="top">15/12/2011 14:25</td><td valign="top"><a class="fancybox  various fancybox.iframe" href="https://maps.google.es/maps?q=calle+de+princesa+90,+madrid&hl=es&sll=40.434627,-3.719068&sspn=0.009032,0.021136&t=w&z=19&output=embed"><img src="img/marker.png"  alt="mapa" /></a></td><td valign="top">Saltarse señal de STOP</td><td valign="top">100 €</td><td valign="top"></td><td valign="top"><img src="img/ok.png" alt="Multa pagada" /> Pagada</td></tr>
                                        <tr><td  valign="top">25/10/2010 09:35</td><td valign="top"><a class="fancybox  various fancybox.iframe" href="https://maps.google.es/maps?f=q&amp;source=s_q&amp;hl=es&amp;geocode=&amp;q=calle+de+ferrocarril+2,+madrid&amp;aq=&amp;sll=40.400713,-3.694679&amp;sspn=0.00113,0.002642&amp;t=w&amp;ie=UTF8&amp;hq=&amp;ll=40.400977,-3.694351&amp;spn=0.00113,0.002642&amp;z=18&amp;output=embed"><img src="img/marker.png"  alt="mapa" /></a></td><td valign="top">Hablar por teléfono móvil</td><td valign="top">200 €</td><td valign="top">-3</td><td valign="top"><img src="img/ok.png" alt="Multa pagada" /> Pagada</td></tr>
                                        <tr><td  valign="top">04/02/2010 13:00</td><td valign="top"><a class="fancybox  various fancybox.iframe" href="https://maps.google.es/maps?q=Carretera+de+Robledo,+San+Lorenzo+de+El+Escorial&hl=es&sll=40.570744,-4.175107&sspn=0.036054,0.117245&t=w&z=18&output=embed"><img src="img/marker.png"  alt="mapa" /></a></td><td valign="top">Adelantamiento con línea contínua</td><td valign="top">200 €</td><td valign="top">-2</td><td valign="top"><img src="img/ok.png" alt="Multa pagada" /> Pagada</td></tr>
                                        <tr><td  valign="top">02/01/2010 22:10</td><td valign="top"><a class="fancybox  various fancybox.iframe" href="https://maps.google.es/?ie=UTF8&amp;ll=40.586044,-4.413908&amp;spn=0.018023,0.042272&amp;t=w&amp;z=15&amp;output=embed"><img src="img/marker.png"  alt="mapa" /></a></td><td valign="top">Conducción temeraria</td><td valign="top">150 €</td><td valign="top"></td><td valign="top"><img src="img/ok.png" alt="Multa pagada" /> Pagada</td></tr>
                                        <tr><td  valign="top">03/09/2009 23:15</td><td valign="top"><a class="fancybox  various fancybox.iframe" href="https://maps.google.es/maps?f=q&amp;source=s_q&amp;hl=es&amp;geocode=&amp;sll=40.487511,-3.875411&amp;sspn=0.009025,0.021136&amp;t=w&amp;ie=UTF8&amp;hq=&amp;z=18&amp;ll=40.487511,-3.875411&amp;output=embed"><img src="img/marker.png"  alt="mapa" /></a></td><td valign="top">No llevar luces puestas</td><td valign="top">100 €</td><td valign="top"></td><td valign="top"><img src="img/ok.png" alt="Multa pagada" /> Pagada</td></tr>
                                        <tr><td  valign="top">16/04/2008 13:54</td><td valign="top"><a class="fancybox  various fancybox.iframe" href="https://maps.google.es/?ie=UTF8&amp;ll=41.596564,-4.8224&amp;spn=0.002218,0.005284&amp;t=w&amp;z=18&amp;output=embed"><img src="img/marker.png"  alt="mapa" /></a></td><td valign="top">Exceso de velocidad</td><td valign="top">200 €</td><td valign="top"></td><td valign="top"><img src="img/ok.png" alt="Multa pagada" /> Pagada</td></tr>
                                    </tbody>
                                    
                                </table>
                            </div>
                        </div>
                        <div class="tabBottom"></div>
                    </div>
                   <div class="tab" id="tab14">
                        <div class="tabcontentwhite">
                             <table cellpadding="0" cellspacing="0" border="0" style="width:720px;table-layout:fixed">
                                <caption style="color:Red;">Se requieren las siguientes acciones:</caption>
                                <thead><tr><th style="width:100px;">Fecha</th><th  style="width:30px;">Lugar</th><th>Vehículo</th><th nowrap="nowrap" style="width:200px;">Se requiere</th><th  style="width:70px;"></th></tr></thead>
                                <tbody>
                                    <tr><td valign="top"><span id="date03"></span> 12:45</td><td valign="top"><a class="fancybox  various fancybox.iframe" href="https://maps.google.es/maps?f=q&amp;source=s_q&amp;hl=es&amp;geocode=&amp;q=carretera+de+madrid,+segovia&amp;aq=&amp;sll=40.929791,-4.109488&amp;sspn=0.028046,0.066047&amp;ie=UTF8&amp;hq=&amp;hnear=Carretera+de+Madrid,+40006+Segovia,+Castilla+y+Le%C3%B3n&amp;ll=40.927589,-4.109419&amp;spn=0.014023,0.033023&amp;t=h&amp;z=16&amp;iwloc=A&amp;output=embed"><img src="img/marker.png"  alt="mapa" /></td><td valign="top">FIAT Seicento</td><td valign="top">Identificar al conductor</td><td><a href="javascript:IdentificarAlConductor();" >Identificar</a></td></tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="tabBottom"></div>
                    </div>
                   <div class="tab" id="tab15">
                        <div class="tabcontentwhite" id="contentVehicles">
                             <table cellpadding="0" cellspacing="0" border="0" style="width:720px;">
                                <caption>Vehículos inscritos a tu nombre</caption>
                                <thead><tr><th>Matrícula</th><th>Modelo</th><th>Itv</th><th>Seguro</th><th></th></tr></thead>
                                <tbody>
                                    <tr><td><a href="javascript:void(0);" rel="res/matricula2.html">3686 GBC</a></td><td><a href="javascript:void(0);" rel="res/car2.html">FIAT Seicento</a></td><td><a href="javascript:void(0);" rel="res/itv2.html" style="color:Red;"><img alt="" src="img/cancel.png" style="vertical-align:text-bottom;margin-right:6px;"  />Caducada</a></td><td><a href="javascript:void(0);" rel="res/poliza2.html"><img src="img/ok.png" alt="" style="margin-right:6px;vertical-align:text-bottom;" />Vigente</a></td><td align="right"><a style="display:none;" href="javascript:TransferirVehiculo();">Transferir</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:DarDeBajaVehiculo();">Dar de baja</a></td></tr>
                                    <tr><td><a href="javascript:void(0);" rel="res/matricula3.html">8240 HFH</a></td><td><a href="javascript:void(0);" rel="res/car3.html">Piaggio X Evo 125</a></td><td><a href="javascript:void(0);" rel="res/itv3.html"><img alt="" src="img/ok.png" style="vertical-align:text-bottom;margin-right:6px;" />Revisada</a></td><td><a href="javascript:void(0);" rel="res/poliza3.html" style="color:Red;"><img src="img/cancel.png" alt="" style="margin-right:6px;vertical-align:text-bottom;" />Caducado</a></td><td align="right"><a href="javascript:TransferirVehiculo();">Transferir</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:DarDeBajaVehiculo();">Dar de baja</a></td></tr>
                                </tbody>
                                
                            </table>
                            <p>
                                <a href="javascript:EsSoloUnaIdea();"><img src="img/add.png" alt="" style="vertical-align:text-bottom;margin-right:7px;" />Dar de alta un vehículo</a>
                            </p>
                        </div>
                        <div class="tabBottom"></div>
                    </div>
      
                </div>
		    </div>
		    <div id="impuestos" class="panel">
			    <div class="content">
				    <h3>Impuestos</h3>
				   <p>With the general sibling selector we can change the color of the "selected" panel link.</p>
			    </div>
		    </div>
            <div id="justicia" class="panel">
			    <div class="content">
				    <h3>Justicia</h3>
				   <p>With the general sibling selector we can change the color of the "selected" panel link.</p>
			    </div>
		    </div>
            <div id="educacion" class="panel">
			    <div class="content">
				    <h3>Educación</h3>
				   <p>With the general sibling selector we can change the color of the "selected" panel link.</p>
			    </div>
		    </div>
            <div id="trabajo" class="panel">
			    <div class="content">
				    <h3>Trabajo</h3>
				   <p>With the general sibling selector we can change the color of the "selected" panel link.</p>
			    </div>
		    </div>
            <div id="seguridadsocial" class="panel">
			    <div class="content">
				    <h3>Seguridad social</h3>
				     <ul class="tabs tabs2 fixed">
                        
                        <li><a class="tabLeft" href="#tab22">Salud</a></li>                    
                        <li><a class="tabCenter" href="#tab23">Trabajo</a></li>
                        <li><a class="tabCenter" href="#tab24">Jubilación</a></li>                        
                        <li><a class="tabRight" href="#tab21">Cotización</a></li>                    
                   </ul>
                    
                    <div class="tab" id="tab21">
                        <div class="tabcontentwhite">
                            <h4>
                               Base actual de cotización: <b>850,20  €</b>.
                            </h4>
                            <div style="margin:15px 0 15px 0;">
                                <div class="captionText" style="margin:7px 0;" id="timespan1"></div>
                                <script type="text/javascript" src="//ajax.googleapis.com/ajax/static/modules/gviz/1.0/chart.js"> {"dataSourceUrl":"//docs.google.com/spreadsheet/tq?key=0Ap9-oaUaqLBxdC1EdUx1TlBPSXFWeDdrNjhCdDhqSlE&transpose=0&headers=1&range=A1%3AD101&gid=0&pub=1","options":{"vAxes":[{"useFormatFromData":true,"viewWindowMode":"pretty","viewWindow":{}},{"useFormatFromData":true,"viewWindowMode":"pretty","viewWindow":{}}],"booleanRole":"certainty","animation":{"duration":0},"domainAxis":{"direction":-1},"legend":"in","theme":"maximized","hAxis":{"useFormatFromData":true,"viewWindowMode":"pretty","viewWindow":{}},"isStacked":false,"width":530,"height":371},"state":{},"chartType":"AreaChart","chartName":"Gr\u00e1fico 1"} </script>
                            </div>
                                
                            <a onclick="DatosDeCotizacion();" href="#tablaDatosCotizacion" title="Mostrar la tabla de datos cotización" class="toggle id tooltip" ><img src="img/plus.png" style="vertical-align:text-bottom;margin-right:6px;" id="imgTablaDatosCotizacion" alt="" />Tabla de datos</a>
                            <div id="tablaDatosCotizacion" style="display:none;">
                                        
                                    <table cellpadding="0" cellspacing="0" border="0" style="width:530px;margin-top:10px;">
                                        <thead><tr><th>Periodo</th><th>Base</th><th>Cuota</th><th>Recargo</th></tr></thead>
                                        <tbody>
                                            <tr><td>2012&nbsp;&nbsp;Septiembre</td><td>850,20</td><td>254,21</td><td>0,00</td></tr>
                                            <tr><td>2012&nbsp;&nbsp;Agosto</td><td>850,20</td><td>254,21</td><td>0,00</td></tr>
                                            <tr><td>2012&nbsp;&nbsp;Julio</td><td>850,20</td><td>254,21</td><td>0,00</td></tr>
                                            <tr><td>2012&nbsp;&nbsp;Junio</td><td>850,20</td><td>254,21</td><td>0,00</td></tr>
                                            <tr><td>2012&nbsp;&nbsp;Mayo</td><td>850,20</td><td>254,21</td><td>0,00</td></tr>
                                            <tr><td>2012&nbsp;&nbsp;Abril</td><td>850,20</td><td>254,21</td><td>0,00</td></tr>
                                            <tr><td>2012&nbsp;&nbsp;Marzo</td><td>850,20</td><td>254,21</td><td>0,00</td></tr>
                                            <tr><td>2012&nbsp;&nbsp;Febrero</td><td>850,20</td><td>254,21</td><td>0,00</td></tr>
                                            <tr><td>2012&nbsp;&nbsp;Enero</td><td>850,20</td><td>254,21</td><td>0,00</td></tr>
                                        
                                            <tr style="border-top:2px solid lightgray;"><td>2011&nbsp;&nbsp;Diciembre</td><td>850,20</td><td>254,21</td><td>0,00</td></tr>
                                            <tr><td>2011&nbsp;&nbsp;Noviembre</td><td>850,20</td><td>254,21</td><td>0,00</td></tr>
                                            <tr><td>2011&nbsp;&nbsp;Octubre</td><td>850,20</td><td>254,21</td><td>0,00</td></tr>
                                            <tr><td>2011&nbsp;&nbsp;Septiembre</td><td>850,20</td><td>254,21</td><td>0,00</td></tr>
                                            <tr><td>2011&nbsp;&nbsp;Agosto</td><td>850,20</td><td>254,21</td><td>0,00</td></tr>
                                            <tr><td>2011&nbsp;&nbsp;Julio</td><td>850,20</td><td>254,21</td><td>0,00</td></tr>
                                            <tr><td>2011&nbsp;&nbsp;Junio</td><td>850,20</td><td>254,21</td><td>0,00</td></tr>
                                            <tr><td>2011&nbsp;&nbsp;Mayo</td><td>850,20</td><td>254,21</td><td>0,00</td></tr>
                                            <tr><td>2011&nbsp;&nbsp;Abril</td><td>850,20</td><td>254,21</td><td>0,00</td></tr>
                                            <tr><td>2011&nbsp;&nbsp;Marzo</td><td>850,20</td><td>254,21</td><td>0,00</td></tr>
                                            <tr><td>2011&nbsp;&nbsp;Febrero</td><td>850,20</td><td>254,21</td><td>0,00</td></tr>
                                            <tr><td>2011&nbsp;&nbsp;Enero</td><td>850,20</td><td>254,21</td><td>0,00</td></tr>

                                            <tr style="border-top:2px solid lightgray;"><td>2010&nbsp;&nbsp;Diciembre</td><td>841,80</td><td>257,70</td><td>0,00</td></tr>
                                            <tr><td>2010&nbsp;&nbsp;Noviembre</td><td>841,80</td><td>257,70</td><td>0,00</td></tr>
                                            <tr><td>2010&nbsp;&nbsp;Octubre</td><td>841,80</td><td>257,70</td><td>0,00</td></tr>
                                            <tr><td>2010&nbsp;&nbsp;Septiembre</td><td>841,80</td><td>257,70</td><td>0,00</td></tr>
                                            <tr><td>2010&nbsp;&nbsp;Agosto</td><td>841,80</td><td>257,70</td><td>0,00</td></tr>
                                            <tr><td>2010&nbsp;&nbsp;Julio</td><td>841,80</td><td>257,70</td><td>0,00</td></tr>
                                            <tr><td>2010&nbsp;&nbsp;Junio</td><td>841,80</td><td>257,70</td><td>0,00</td></tr>
                                            <tr><td>2010&nbsp;&nbsp;Mayo</td><td>841,80</td><td>257,70</td><td>0,00</td></tr>
                                            <tr><td>2010&nbsp;&nbsp;Abril</td><td>841,80</td><td>257,70</td><td>0,00</td></tr>
                                            <tr><td>2010&nbsp;&nbsp;Marzo</td><td>841,80</td><td>257,70</td><td>0,00</td></tr>
                                            <tr><td>2010&nbsp;&nbsp;Febrero</td><td>841,80</td><td>257,70</td><td>0,00</td></tr>
                                            <tr><td>2010&nbsp;&nbsp;Enero</td><td>841,80</td><td>257,70</td><td>0,00</td></tr>

                                            <tr style="border-top:2px solid lightgray;"><td>2009&nbsp;&nbsp;Diciembre</td><td>833,40</td><td>249,18</td><td>0,00</td></tr>
                                            <tr><td>2009&nbsp;&nbsp;Noviembre</td><td>833,40</td><td>249,18</td><td>0,00</td></tr>
                                            <tr><td>2009&nbsp;&nbsp;Octubre</td><td>833,40</td><td>249,18</td><td>0,00</td></tr>
                                            <tr><td>2009&nbsp;&nbsp;Septiembre</td><td>833,40</td><td>249,18</td><td>0,00</td></tr>
                                            <tr><td>2009&nbsp;&nbsp;Agosto</td><td>833,40</td><td>249,18</td><td>0,00</td></tr>
                                            <tr><td>2009&nbsp;&nbsp;Julio</td><td>833,40</td><td>249,18</td><td>0,00</td></tr>
                                            <tr><td>2009&nbsp;&nbsp;Junio</td><td>833,40</td><td>249,18</td><td>0,00</td></tr>
                                            <tr><td>2009&nbsp;&nbsp;Mayo</td><td>833,40</td><td>249,18</td><td>0,00</td></tr>
                                            <tr><td>2009&nbsp;&nbsp;Abril</td><td>833,40</td><td>249,18</td><td>0,00</td></tr>
                                            <tr><td>2009&nbsp;&nbsp;Marzo</td><td>833,40</td><td>249,18</td><td>0,00</td></tr>
                                            <tr><td>2009&nbsp;&nbsp;Febrero</td><td>833,40</td><td>249,18</td><td>0,00</td></tr>
                                            <tr><td>2009&nbsp;&nbsp;Enero</td><td>833,40</td><td>249,18</td><td>0,00</td></tr>

                                        
                                            <tr style="border-top:2px solid lightgray;"><td>2008&nbsp;&nbsp;Diciembre</td><td>817,20</td><td>244,35</td><td>0,00</td></tr>
                                            <tr><td>2008&nbsp;&nbsp;Noviembre</td><td>817,20</td><td>244,35</td><td>0,00</td></tr>
                                            <tr><td>2008&nbsp;&nbsp;Octubre</td><td>817,20</td><td>244,35</td><td>0,00</td></tr>
                                            <tr><td>2008&nbsp;&nbsp;Septiembre</td><td>817,20</td><td>244,35</td><td>0,00</td></tr>
                                            <tr><td>2008&nbsp;&nbsp;Agosto</td><td>817,20</td><td>244,35</td><td>0,00</td></tr>
                                            <tr><td>2008&nbsp;&nbsp;Julio</td><td>817,20</td><td>244,35</td><td>0,00</td></tr>
                                            <tr><td>2008&nbsp;&nbsp;Junio</td><td>817,20</td><td>244,35</td><td>0,00</td></tr>
                                            <tr><td>2008&nbsp;&nbsp;Mayo</td><td>817,20</td><td>244,35</td><td>0,00</td></tr>
                                            <tr><td>2008&nbsp;&nbsp;Abril</td><td>817,20</td><td>244,35</td><td>0,00</td></tr>
                                            <tr><td>2008&nbsp;&nbsp;Marzo</td><td>817,20</td><td>244,35</td><td>0,00</td></tr>
                                            <tr><td>2008&nbsp;&nbsp;Febrero</td><td>817,20</td><td>244,35</td><td>0,00</td></tr>
                                            <tr><td>2008&nbsp;&nbsp;Enero</td><td>817,20</td><td>244,35</td><td>0,00</td></tr>

                                            <tr style="border-top:2px solid lightgray;"><td>2007&nbsp;&nbsp;Diciembre</td><td>801,30</td><td>212,34</td><td>0,00</td></tr>
                                            <tr><td>2007&nbsp;&nbsp;Noviembre</td><td>801,30</td><td>212,34</td><td>0,00</td></tr>
                                            <tr><td>2007&nbsp;&nbsp;Octubre</td><td>801,30</td><td>212,34</td><td>0,00</td></tr>
                                            <tr><td>2007&nbsp;&nbsp;Septiembre</td><td>801,30</td><td>159,25</td><td>0,00</td></tr>
                                            <tr><td>2007&nbsp;&nbsp;Agosto</td><td>801,30</td><td>159,25</td><td>0,00</td></tr>
                                            <tr><td>2007&nbsp;&nbsp;Julio</td><td>801,30</td><td>159,25</td><td>0,00</td></tr>
                                            <tr><td>2007&nbsp;&nbsp;Junio</td><td>801,30</td><td>159,25</td><td>0,00</td></tr>
                                            <tr><td>2007&nbsp;&nbsp;Mayo</td><td>801,30</td><td>159,25</td><td>0,00</td></tr>
                                            <tr><td>2007&nbsp;&nbsp;Abril</td><td>801,30</td><td>159,25</td><td>0,00</td></tr>
                                            <tr><td>2007&nbsp;&nbsp;Marzo</td><td>801,30</td><td>159,25</td><td>0,00</td></tr>
                                            <tr><td>2007&nbsp;&nbsp;Febrero</td><td>801,30</td><td>159,25</td><td>0,00</td></tr>
                                            <tr><td>2007&nbsp;&nbsp;Enero</td><td>801,30</td><td>159,25</td><td>0,00</td></tr>

                                            <tr style="border-top:2px solid lightgray;"><td>2006&nbsp;&nbsp;Diciembre</td><td>785,70</td><td>156,16</td><td>0,00</td></tr>
                                            <tr><td>2006&nbsp;&nbsp;Noviembre</td><td>785,70</td><td>156,16</td><td>0,00</td></tr>
                                            <tr><td>2006&nbsp;&nbsp;Octubre</td><td>785,70</td><td>156,16</td><td>0,00</td></tr>
                                            <tr><td>2006&nbsp;&nbsp;Septiembre</td><td>785,70</td><td>156,16</td><td>0,00</td></tr>
                                            <tr><td>2006&nbsp;&nbsp;Agosto</td><td>785,70</td><td>156,16</td><td>0,00</td></tr>
                                            <tr><td>2006&nbsp;&nbsp;Julio</td><td>785,70</td><td>156,16</td><td>0,00</td></tr>
                                            <tr><td>2006&nbsp;&nbsp;Junio</td><td>785,70</td><td>156,16</td><td>0,00</td></tr>
                                            <tr><td>2006&nbsp;&nbsp;Mayo</td><td>785,70</td><td>156,16</td><td>0,00</td></tr>
                                            <tr><td>2006&nbsp;&nbsp;Abril</td><td>785,70</td><td>156,16</td><td>0,00</td></tr>
                                            <tr><td>2006&nbsp;&nbsp;Marzo</td><td>785,70</td><td>156,16</td><td>0,00</td></tr>
                                            <tr><td>2006&nbsp;&nbsp;Febrero</td><td>785,70</td><td>156,16</td><td>0,00</td></tr>
                                            <tr><td>2006&nbsp;&nbsp;Enero</td><td>785,70</td><td>156,16</td><td>0,00</td></tr>

                                            <tr style="border-top:2px solid lightgray;"><td>2005&nbsp;&nbsp;Diciembre</td><td>770,40</td><td>153,12</td><td>0,00</td></tr>
                                            <tr><td>2005&nbsp;&nbsp;Noviembre</td><td>770,40</td><td>153,12</td><td>0,00</td></tr>
                                            <tr><td>2005&nbsp;&nbsp;Octubre</td><td>770,40</td><td>153,12</td><td>0,00</td></tr>
                                                
                                        
                                        

                                        </tbody>
                                    </table>
                                       
                                </div>
                            <div style="margin:20px 0 25px 0;">
                                Cambiar la <a href="javascript:CambiarBaseCotizacion();">base de cotización de autónomos</a>.<br />
                                Solicitar la <a href="javascript:BajaAutonomo();">baja como autónomo</a>.<br />
                                ¿Hay algún error en la base de cotización? <a href="javascript:RectificacionCotizacion();">Pedir rectificación</a>.<br />
                                
                            </div>
                        </div>
                        <div class="tabBottom"></div>
                    </div>
                    <div class="tab" id="tab22">
                        <div class="tabcontentwhite" id="sanidad">
                            <h4>Tu médico de cabecera es <a  href="javascript:void(0);" rel="res/medicocabecera.html">Maria Luisa González</a>.</h4>
                            <form style="margin-top:20px;" action="javascript:EsSoloUnaIdea();">
                                <button type="submit">Pedir cita con el médico</button>
                            </form>
                            
                            <!--a onclick="HistoricoVacunas();" href="#tablaHistorialDeVacunacion" title="Mostrar el historial de vacunación" class="toggle id tooltip" ><img src="img/plus.png" style="vertical-align:text-bottom;margin-right:6px;" id="imgHistorialVacunacion" alt="" />Historial de vacunación</a>
                            <div id="tablaHistorialDeVacunacion" style="display:none;margin-top:10px;"-->
                            <div id="tablaHistorialDeVacunacion">
                                <table cellpadding="0" cellspacing="0" border="0" style="width:720px;table-layout:fixed;">
                                    <caption>Historial de vacunación</caption>
                                    <thead>
                                        <tr><th style="width:70px;">Fecha</th><th style="width:30px;">Centro</th><th>Vacunas</th><th style="width:180px;">Observaciones</th></tr>
                                    </thead>
                                    <tbody>
                                        
                                        <tr><td  valign="top">28/07/1994</td><td valign="top">..</td><td valign="top">Tétanos-difteria tipo adulto, Virus Papiloma Humano</td><td valign="top"></td></tr>
                                        <tr><td  valign="top">20/07/1991</td><td valign="top">..</td><td valign="top">Varicela</td><td valign="top"></td></tr>
                                        <tr><td  valign="top">25/07/1984</td><td valign="top">..</td><td valign="top">Difteria-Tétanos-Tos ferina acelular, Sarampión-Rubéola-Parotiditis</td><td valign="top"></td></tr>
                                        <tr><td  valign="top">27/03/1982</td><td valign="top">..</td><td valign="top">Difteria-Tétanos-Tos ferina acelular, Haemophilus Influenzae b, Polio</td><td valign="top"></td></tr>
                                        <tr><td  valign="top">26/12/1981</td><td valign="top">..</td><td valign="top">Rubéola-Parotiditis-Sarampión, Meningococo C, Varicela</td><td valign="top"></td></tr>
                                        <tr><td  valign="top">21/03/1981</td><td valign="top">..</td><td valign="top">Hepatitis B, Difteria, Tétanos, Tor ferina, Influenzae b, Polio inactiva</td><td valign="top"></td></tr>
                                        <tr><td  valign="top">18/01/1981</td><td valign="top">..</td><td valign="top">Hepatitis B, Difteria, Tétanos, Tor ferina, Influenzae b, Polio inactiva, Meningococo c</td><td valign="top"></td></tr>
                                        <tr><td  valign="top">20/09/1980</td><td valign="top">..</td><td valign="top">Hepatitis B, Difteria, Tétanos, Tor ferina, Influenzae b, Polio inactiva, Meningococo c</td><td valign="top"></td></tr>
                                        <tr><td  valign="top">20/07/1980</td><td valign="top">..</td><td valign="top">Hepatitis B</td><td valign="top"></td></tr>
                                    </tbody>
                                    
                                </table>
                            </div>

                        </div>
                        <div class="tabBottom"></div>
                    </div>
                    <div class="tab" id="tab23">
                        <div class="tabcontentwhite">
                            <h4>
                                    Tu situación como trabajador: <img alt="" src="img/ok.png" style="vertical-align:text-bottom;margin-right:6px;" /><span style="color:Green;font-weight:bold;">Activo</span> desde el 28/09/2004.
                            </h4>
                           
                            <div style="margin:10px 0 40px 0;">
                                <table  cellpadding="0" cellspacing="0" border="0" style="width:560px;">
                                    <caption id="timespan4"></caption>
                                    <thead><tr><th>Régimen</th><th>Empresa</th><th>Periodo</th><th>Días</th></tr></thead>
                                    <tbody>
                                        <tr><td>Autónomo</td><td>Autónomo: Actividades de ingeniería</td><td>1/10/2009 - Hoy</td><td><span id="timeSpan3"></span></td></tr>
                                        <tr><td>General</td><td>Telecom Universe S.L.</td><td>1/09/2005 - 30/09/2009</td><td>1490</td></tr>
                                        <tr><td>General</td><td>Implemental Systems S.L.</td><td>28/09/2004 - 15/08/2005</td><td>322</td></tr>
                                    </tbody>
                                </table>
                            </div>
                            
                            <div style="margin:20px 0 25px 0;">
                                ¿Hay algún error? <a href="javascript:RectificacionVidaLaboral();">Pedir rectificación</a>.
                            </div>
                        </div>
                        <div class="tabBottom"></div>
                    </div>
                    <div class="tab" id="tab24">
                        <div class="tabcontentwhite">
                            <h4>Podrás jubilarte cuando cumplas <b>67</b> años, te quedan <b><span id="timeSpan2"></span></b> días.</h4>
                        </div>
                        <div class="tabBottom"></div>
                    </div>
			    </div>
		    </div>
            <div id="notificaciones" class="panel">
			    <div class="content">
				    <h3>Notificaciones</h3>
				   <p>With the general sibling selector we can change the color of the "selected" panel link.</p>
			    </div>
		    </div>
            
            <div id="datospersonales" class="panel">
			    <div class="content">
				    <h3>Antonio García Rubio</h3>
				    <ul class="tabs fixed">
                        <li><a href="#tab1">Datos personales</a></li>
                        <li><a href="#tab3">Notificaciones</a></li>
                        <li><a href="#tab2">Datos bancarios</a></li>                    
                    </ul>
      
                    <div class="tab" id="tab1">
                    <div class="tabcontent">
                        <table cellpadding="0" cellspacing="0" border="0">
                            <thead><tr style="display:none;"><th></th><th></th></tr></thead>
                            <tbody>
                                <tr class="trNoBottomBorder"><td valign="top" style="min-width:130px;width:1%;"><b>Fotografía:</b></td><td><img class="imagedropshadow" alt="" src="img/fotocarnetgrande.jpg" style="margin-bottom:10px;" /></td></tr>
                                <tr><td><b>Nombre:</b></td><td >Antonio</td></tr>
                                <tr><td><b>Apellidos:</b></td><td>García Rubio</td></tr>
                                <tr><td><b>Nacimiento:</b></td><td>15/07/1980 en Madrid (España)</td></tr>    
                                <tr><td><b>Hijo de:</b></td><td>Fernando y Maria Jose. <a class="fancybox" href="img/genealogia.png">Mi árbol genealógico</a>.</td></tr><!-- Arbol genealogico creado con familyecho.com-->
                                <tr><td><b>DNI:</b></td><td>50925433J&nbsp;&nbsp;&nbsp;Expedido el 3/4/2010. Válido hasta el 3/4/2015. <a href="javascript:PedirDNIPasaporte(true);">Pedir un DNI nuevo</a>.</td></tr>  
                                <tr><td><b>Pasaporte:</b></td><td>AAD1258865&nbsp;&nbsp;&nbsp;Expedido el 8/2/2011. Válido hasta el 8/2/2016. <a href="javascript:PedirDNIPasaporte(false);" >Pedir un pasaporte  nuevo</a>.</td></tr>  
                                <tr><td><b>Domicilio:</b></td><td>C/Castillo de Fuensaldaña 4 - 211, 28290, Las Rozas de Madrid (Madrid). <a href="javascript:ModificarDirección();">Cambiar de domicilio</a>.</td></tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="tabBottom"></div>
                  </div>

                    <div class="tab" id="tab2">
                     <div class="tabcontent">
                        <table cellpadding="0" cellspacing="0" border="0">
                            <thead><tr style="display:none;"><th></th><th></th></tr></thead>
                            <tbody>
                                <tr><td style="min-width:240px;width:1%;"><b>Cuenta principal:</b></td><td>2100 5801 95 02 00096756 &nbsp;&nbsp;<img alt="La cuenta está verificada" src="img/ok.png" style="vertical-align:text-bottom;" />&nbsp;<b>Verificada</b>&nbsp;&nbsp;<a href="javascript:FancyConfirm('¿Quieres borrar la cuenta bancaria principal?','Tu cuenta secundaria, pasará a funcionar como cuenta principal.');">Eliminar</a></td></tr>
                                <tr><td><b>Cuenta secundaria:</b></td><td>0227 0003 76 02 06152213 &nbsp;&nbsp;<img alt="La cuenta está verificada" src="img/ok.png" style="vertical-align:text-bottom;" />&nbsp;<b>Verificada</b>&nbsp;&nbsp;<a href="javascript:FancyConfirm('¿Quieres borrar la cuenta bancaria secundaria?','Todos los ingresos y domiciliaciones asociados a esta cuenta, pasarán a asociarse con la cuenta principal.');">Eliminar</a></td></tr>
                                <tr><td><b>Tarjeta:</b></td><td>XXXX XXXX XXXX XXXX 6873 &nbsp;&nbsp;<img alt="La cuenta está verificada" src="img/waiting.png" style="vertical-align:text-bottom;" />&nbsp;<b>Pendiente de verificar</b>&nbsp;&nbsp;<a href="javascript:FancyConfirm('¿Quieres borrar esta tarjeta?','Los cobros que tengas asociados con esta tarjeta, pasarán a realizarse con tu cuenta bancaria principal.');">Eliminar</a></td></tr>
                                <tr  class="trNoBottomBorder"><td></td><td><img src="img/add.png" alt="Añadir cuenta bancaria o tarjeta" style="vertical-align:text-bottom;margin-right:7px;" /><a href="javascript:AnadirCuentaOTarjeta();">Añadir cuenta o tarjeta</a></td></tr>
                                <tr><td class="tdSeparator" colspan="2"></td></tr>
                                <tr><td class="tdEmpty" colspan="2"></td></tr>
                                <tr class="trNoBottomBorder"><td></td><td>PAGAR CON:</td></tr>
                                <tr><td><b>Cotización de la seguridad social:</b></td><td><select style="width:200px;" class="selected"><option selected="selected">Cuenta principal</option><option>Cuenta secundaria</option></select></td></tr>
                                <tr><td><b>Multas de tráfico (pasado el periodo para recurrir):</b></td><td><select style="width:200px;" class="selected"><option selected="selected">Cuenta principal</option><option>Cuenta secundaria</option></select></td></tr>
                                <tr><td><b>Impuesto sobre la renta:</b></td><td><select style="width:200px;" class="selected"><option >Cuenta principal</option><option>Cuenta secundaria</option></select></td></tr>
                                <tr><td><b>Impuesto sobre bienes inmuebles:</b></td><td><select style="width:200px;" class="selected"><option selected="selected">Cuenta principal</option><option>Cuenta secundaria</option></select></td></tr>
                                <tr><td><b>Impuesto sobre vehículos:</b></td><td><select style="width:200px;" class="selected"><option>Cuenta principal</option><option selected="selected">Cuenta secundaria</option></select></td></tr>
                                <tr  class="trNoBottomBorder"><td><b>Impuesto de actividades económicas:</b></td><td><select style="width:200px;" class="selected"><option selected="selected">Cuenta principal</option><option>Cuenta secundaria</option></select></td></tr>
                                <tr><td class="tdSeparator" colspan="2"></td></tr>
                                <tr><td class="tdEmpty" colspan="2"></td></tr>
                                <tr class="trNoBottomBorder"><td></td><td>INGRESAR EN:</td></tr>
                                <tr><td><b>Prestaciones por desempleo:</b></td><td><select style="width:200px;" class="selected"><option selected="selected">Cuenta principal</option><option>Cuenta secundaria</option></select></td></tr>
                                <tr><td><b>Resultado de impuestos a devolver:</b></td><td><select style="width:200px;" class="selected"><option selected="selected">Cuenta principal</option><option>Cuenta secundaria</option></select></td></tr>
                                <tr class="trNoBottomBorder"><td><b>Subvenciones:</b></td><td><select style="width:200px;" class="selected"><option>Cuenta principal</option><option>Cuenta secundaria</option></select></td></tr>
                            </tbody>
                        </table>
                     </div>
                    <div class="tabBottom"></div>
                    
                  </div>
      
                    <div class="tab" id="tab3">
                    <div class="tabcontent">
      	                <table cellpadding="0" cellspacing="0" border="0" id="tablaDatosPersonalesNotificaciones">
                            <thead><tr style="display:none;"><th></th><th></th></tr></thead>
                            <tbody>
                                <tr><td valign="top" style="min-width:185px;width:1%;"><b>Tu email:</b></td><td valign="top">antoniogarciarubio@gmail.com &nbsp;&nbsp;<img alt="La cuenta está verificada" src="img/ok.png" style="vertical-align:text-bottom;" /> <b>Verificado</b>&nbsp;&nbsp;&nbsp;<a href="javascript:ModificarEmail();">Cambiar</a></td></tr>
                                <tr><td valign="top"><b>Tu teléfono móvil:</b></td><td valign="top">+34 647030087 &nbsp;&nbsp;<img alt="La cuenta está verificada" src="img/ok.png" style="vertical-align:text-bottom;" /> <b>Verificado</b>&nbsp;&nbsp;&nbsp;<a href="javascript:ModificarTelefono();">Cambiar</a></td></tr>
                                <tr  class="trNoBottomBorder"><td valign="top"><b>Tu dirección postal:</b></td><td valign="top">C/ Castillo de Fuensaldaña 4 - 211, 28290, Las Rozas de Madrid (Madrid) &nbsp;&nbsp;<img style="vertical-align:text-bottom; alt="Pendiente" src="img/waiting.png" /> <b>Pendiente de verificar</b></td></tr>
                                <tr><td class="tdSeparator" colspan="2"></td></tr>
                                  </tbody>
                        </table>
                        <table cellpadding="0" cellspacing="0" border="0" style="width:1px;" id="Table1">
                            <thead><tr style="display:none;"><th></th><th></th></tr></thead>
                            <tbody>
                                <tr class="trNoBottomBorder"><td ></td><td colspan="3" align="center"><b>Notificarme por:</b></td></tr>
                                <tr class="trNoBottomBorder"><td  style="min-width:185px;width:185px;"></td><td align="center" style="width: 60px;">Email</td><td valign="top" align="center" style="width:60px;">SMS</td><td valign="top" align="center"  style="width:80px;" nowrap>Carta <img alt="" title="Piensa en el medioambiente y en el coste que nos supone a todos enviarte cartas." src="img/leaf.png" style="vertical-align:text-bottom;" /></td></tr>
                                <tr><td ><b>Cargos en tu cuenta bancaria:</b></td><td valign="top"  align="center"><input type="checkbox" name="checkbox" value="1" class="required" checked="checked"  /></td><td valign="top" align="center"><input type="checkbox" name="checkbox" value="1" class="required" /></td><td valign="top" align="center"><input type="checkbox" name="checkbox" value="1" class="required" /></td></tr>
                                <tr><td ><b>Ingresos en tu cuenta bancaria:</b></td><td valign="top"  align="center"><input type="checkbox" name="checkbox" value="1" class="required" checked="checked"  /></td><td valign="top" align="center"><input type="checkbox" name="checkbox" value="1" class="required" checked="checked" /></td><td valign="top" align="center"><input type="checkbox" name="checkbox" value="1" class="required" /></td></tr>
                                <tr><td ><b>Multas y requerimientos de tráfico:</b></td><td  valign="top" align="center"><input type="checkbox" name="checkbox" value="1" class="required" checked="checked"  /></td><td valign="top" align="center"><input type="checkbox" name="checkbox" value="1" class="required" checked="checked"  /></td><td valign="top" align="center"><input type="checkbox" name="checkbox" value="1" class="required" checked="checked" /></td></tr>
                                <tr><td ><b>Resto de notificaciones de tráfico:</b></td><td valign="top"  align="center"><input type="checkbox" name="checkbox" value="1" checked="checked" class="required" /></td><td valign="top" align="center"><input type="checkbox" name="checkbox" value="1" class="required"   /></td><td valign="top" align="center"><input type="checkbox" name="checkbox" value="1" class="required" /></td></tr>
                                <tr><td ><b>Cambios en tu base de cotización de seguridad social:</b></td><td valign="top"  align="center"><input type="checkbox" name="checkbox" value="1" checked="checked" class="required" /></td><td valign="top" align="center"><input type="checkbox" name="checkbox" value="1" class="required"   /></td><td valign="top" align="center"><input type="checkbox" name="checkbox" value="1" class="required" /></td></tr>
                                <tr><td ><b>Denuncias recibidas:</b></td><td  valign="top" align="center"><input type="checkbox" name="checkbox" value="1" checked="checked" class="required" /></td><td valign="top" align="center"><input type="checkbox" name="checkbox" value="1" class="required" checked="checked"  /></td><td valign="top" align="center"><input type="checkbox" name="checkbox" value="1" class="required" /></td></tr>
                                <tr class="trNoBottomBorder"><td ><b>Citaciones con la administración pública:</b></td><td  valign="top" align="center"><input type="checkbox" name="checkbox" value="1" checked="checked" class="required" /></td><td valign="top" align="center"><input type="checkbox" name="checkbox" value="1" class="required"   /></td><td valign="top" align="center"><input type="checkbox" name="checkbox" value="1" class="required" /></td></tr>
                                
                            </tbody>
                        </table>
					</div>
                    <div class="tabBottom"></div>

                  </div>
              
			    </div>
		    </div>
		    
        </div>	
		
	</div>	
	
	<!--div id="footer">
		<p>Made with <a href="http://easyframework.com">Easy</a></p>
	</div-->

</div>


<!--script type="text/javascript">
    var $buoop = {}
    $buoop.ol = window.onload;
    window.onload = function () {
        try { if ($buoop.ol) $buoop.ol(); } catch (e) { }
        var e = document.createElement("script");
        e.setAttribute("type", "text/javascript");
        e.setAttribute("src", "http://browser-update.org/update.js");
        document.body.appendChild(e);
    } 
</script--> 

<script type='text/javascript'>

    var _ues = {
        host: 'sentidocomun.userecho.com',
        forum: '14393',
        lang: 'es',
        tab_corner_radius: 5,
        tab_font_size: 14,
        tab_image_hash: 'Q29tZW50YXJpb3M%3D',
        tab_alignment: 'right',
        tab_text_color: '#FFFFFF',
        tab_bg_color: '#CC3300',
        tab_hover_color: '#F45C5C'
    };

    (function () {
        var _ue = document.createElement('script'); _ue.type = 'text/javascript'; _ue.async = true;
        _ue.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.userecho.com/js/widget-1.4.gz.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(_ue, s);
    })();

</script>

</body>
</html>