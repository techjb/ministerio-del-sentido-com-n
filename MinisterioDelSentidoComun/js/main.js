﻿var selectedMenu = false;
var selectedSubMenu = false;
var historicoMultasOpen = false;
var historialVacunasOpen = false;
var tablaDatosCotizacion = false;
var provincias = new Array("A CORUÑA", "ÁLAVA", "ALACANT", "ALBACETE", "ALMERIA", "ASTURIAS", "AVILA", "BADAJOZ", "BARCELONA", "BURGOS", "CACERES", "CADIZ", "CANTABRIA", "CASTELLO", "CEUTA", "CIUDAD REAL", "CORDOBA", "CUENCA", "GIRONA", "GRANADA", "GUADALAJARA", "GUIPÚZCOA", "HUELVA", "HUESCA", "ILLES BALEARS", "JAEN", "LA RIOJA", "LAS PALMAS", "LEON", "LLEIDA", "LUGO", "MADRID", "MALAGA", "MELILLA", "MURCIA", "NAVARRA", "OURENSE", "PALENCIA", "PONTEVEDRA", "S. C. TENERIFE", "SALAMANCA", "SEGOVIA", "SEVILLA", "SORIA", "TARRAGONA", "TERUEL", "TOLEDO", "VALENCIA", "VALLADOLID", "VIZCAYA", "ZAMORA", "ZARAGOZA");
var codigoProvincias = new Array("15", "1", "3", "2", "4", "33", "5", "6", "8", "9", "10", "11", "39", "12", "51", "13", "14", "16", "17", "18", "19", "20", "21", "22", "7", "23", "26", "35", "24", "25", "27", "28", "29", "52", "30", "31", "32", "34", "36", "38", "37", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50");


$(document).ready(function () {
    $.easy.navigation();
    // $.easy.tooltip();
    //$.easy.popup();
    $.easy.external();
    $.easy.rotate();
    $.easy.cycle();
    $.easy.forms();
    $.easy.showhide();
    $.easy.jump();
    $.easy.tabs();
    $.easy.accordion();
    $(window).resize(function () {ResizeWindow();});
    $(window).bind('hashchange', HasChange);

    ResizeWindow();
    HasChange();
    SetDates();

    LoadQtipWindows('#contentVehicles');
    LoadQtipWindows('#sanidad');

    $('img[title]').qtip({ style: { name: 'dark', tip: true} })
    $('.fancybox').fancybox();
    $('#link-notificaciones')[0].click();


});

function SetDates() {
    var today = new Date.today();
    var bornDay = new Date('07/15/1980');

    var date01 = today.addDays(-4);
    $("#date01").text(date01.toString('dd/MM/yyyy'));
    $("#date02").text(today.addDays(-39).toString('dd/MM/yyyy'));
    $("#date03").text(today.addDays(-25).toString('dd/MM/yyyy'));
    $("#infoMulta1").attr("title", "100 € si lo pagas antes del " + date01.addDays(30).toString('dd/MM/yyyy'));

    var ts1 = new TimeSpan(Date.today() - new Date('09/28/2004'));
    $("#timespan1").text("Has cotizado durante " + ts1.days + " días un total de 18.756,72 €.");

    var ts2 = new TimeSpan(bornDay.addYears(67) - Date.today());
    $("#timeSpan2").text(ts2.days);

    var ts3 = new TimeSpan(Date.today() - new Date('10/01/2009'));
    $("#timeSpan3").text(ts3.days);

    //alert(new TimeSpan(new Date('09/30/2009') - new Date('09/01/2005')).days);
    
    $("#timespan4").text("Has trabajado durante " + (parseInt(ts3.days, 0) + 1490 + 322) + " días.");
     
}

function LoadQtipWindows(id) {
    $(id +' a[rel]').each(function () {
        $(this).qtip(
      {
          content: {
              // Set the text to an image HTML string with the correct src URL to the loading image you want to use
              text: '<img class="throbber" src="/img/loading.gif" alt="Cargando..." />',
              url: $(this).attr('rel'), // Use the rel attribute of each element for the url to load
              title: {
                  text: $(this).text(), // Give the tooltip a title using each elements text
                  button: 'Cerrar' // Show a close link in the title
              }
          },
          position: {
              corner: {
                  target: 'bottomMiddle', // Position the tooltip above the link
                  tooltip: 'topMiddle'
              },
              adjust: {
                  screen: true // Keep the tooltip on-screen at all times
              }
          },
          show: {
              when: 'click',
              solo: true // Only show one tooltip at a time
          },
          hide: 'unfocus',
          style: {
              tip: true, // Apply a speech bubble tip to the tooltip at the designated tooltip corner
              border: {
                  width: 0,
                  radius: 4
              },
              name: 'light', // Use the default light style
              width: '100%'// Set the tooltip width

          }
      })
    });
}



function ResizeWindow() {
    var windowWidth = $(window).width();
    var windowHeight = $(window).height();
    var aux = 0; // esto hay que ponerlo a 0 cuando quitemos el borde
    var aux2 = 20;

    $('body').css("max-height", windowHeight);
    $(".main").css("height", windowHeight - aux2 - $("#header").outerHeight());
    
    var secondaryHeight = $(".main").height();
    var secondaryWidth = windowWidth - aux - $(".main").outerWidth() - parseInt($(".secondary").css("padding-left"));
    $(".secondary").css("width", secondaryWidth);    
    $(".secondary").css("height", secondaryHeight);
    $('.secondary').children().each(function (index) {
            $(this).css("height", secondaryHeight+ aux2);
        });
    $('.tab').each(function (index) {
        $(this).css("height", secondaryHeight - 84);
        $(this).css("width", secondaryWidth+2);
    });
    var paddigLeft =  parseInt($(".secondary").css("padding-left"));
    var paddigRight =  parseInt($(".secondary").css("padding-right"));
    $('.tabcontent').each(function (index) {
       //$(this).css("height", secondaryHeight - 120);
        $(this).css("width", secondaryWidth -paddigLeft - paddigRight- 60);
        
    });
}


function HasChange() {
    var hash = location.hash.replace("#", "") ;
    var isMainMenu = false;
    /*$('.list li a').each(function (index) {
        alert($(this).attr('id'));
        if ($(this).attr('id') == hash) {
            isMainMenu = true;
        }
    });
    if (isMainMenu) alert(hash);*/
    
    if (selectedSubMenu) {
        $(selectedSubMenu).css("color", "black");
        
        if (selectedSubMenu.attr("id") != "link-notificaciones")
            $(selectedSubMenu).css("font-weight", "normal");
        if (selectedSubMenu.attr("id") != "link-datospersonales")
            $(selectedSubMenu).css("border-left", "5px solid white");
    }

    if (hash) {
        selectedSubMenu = $('#link-' + hash);
        $(selectedSubMenu).css("color","#CC3300");
        $(selectedSubMenu).css("font-weight", "bold");
        if (selectedSubMenu.attr("id") != "link-datospersonales")
            $(selectedSubMenu).css("border-left", "5px solid #CC3300");
    }
}

function EsSoloUnaIdea() {

    FancyInfo('Esto es solo una idea.', 'Pero si te ha gustado compartela en tus redes sociales.');
}

function FancyInfo(title, text) {
    $.fancybox({
        'modal': false,
        padding: 40,
        'content': '<div class="fancyConfirm"><h3>'+ title +'</h3><div>'+ text +'</div></div>'
    });
}

function FancyConfirm(title, text) {
    text = text || '';
    if (title != '') title = "<h3>" + title + "</h3>";
    $.fancybox({
        'modal': false,
        padding: 40,
        'content': '<div class="fancyConfirm">' + title + '<div style="margin:20px 0 30px 0;">' + text + '</div><p><button style="margin-right:15px;" onclick="EsSoloUnaIdea();">Aceptar</button><button class="buttonCancel" onclick="$.fancybox.close();">Cancelar</button></p></div>'
    });
}

function HistoricoDeMultas() {
    $('#historicoPlus').attr("src", historicoMultasOpen ? 'img/plus.png' : 'img/minus.png');
    historicoMultasOpen = !historicoMultasOpen;
}

function HistoricoVacunas() {
    $('#imgHistorialVacunacion').attr("src", historialVacunasOpen ? 'img/plus.png' : 'img/minus.png');
    historialVacunasOpen = !historialVacunasOpen;
}

function DatosDeCotizacion() {
    $('#imgTablaDatosCotizacion').attr("src", tablaDatosCotizacion ? 'img/plus.png' : 'img/minus.png');
    tablaDatosCotizacion = !tablaDatosCotizacion;
}

function RectificacionVidaLaboral() {
    var text = '<fieldset><div><label>Empresa</label><select class="select" style="width:100%;"><option>1/10/2009 - Hoy Autónomo: Actividades de ingeniería</option><option>1/09/2005 - 30/09/2009 Telecom Universe S.L.</option><option>28/09/2004 - 15/08/2005 Implemental Systems S.L.</option></select></div><div><label>Fecha de inicio</label><input type="date" class="field required" style="width:120px;" size="10"/></div><div><label>Fecha de cese:</label><input type="date" class="field required" style="width:120px" size="10"/></div><div><label>Observaciones</label><textarea name="message" id="message" cols="30" rows="5" class="area" style="height:70px;width:100%"></textarea></div></fieldset>';
    FancyConfirm('Rectificación en el informe de vida laboral.', text);
}

function RectificacionCotizacion() {
    var text = '<fieldset><div><label>Empresa</label><select class="select" style="width:100%;"><option>1/10/2009 - Hoy Autónomo: Actividades de ingeniería</option><option>1/09/2005 - 30/09/2009 Telecom Universe S.L.</option><option>28/09/2004 - 15/08/2005 Implemental Systems S.L.</option></select></div><div><label>Periodo de liquidación</label><input type="date" class="field required" style="width:120px;" size="10"/></div><div><label>Base de cotización:</label><input type="text" class="field required" style="width:120px;text-align:right;" size="10"/> €</div><div><label>Observaciones</label><textarea name="message" id="message" cols="30" rows="5" class="area" style="height:70px;width:100%"></textarea></div></fieldset>';
    FancyConfirm('Rectificación en la base de cotización.', text);
}

function CambiarBaseCotizacion() {
    var text = '<fieldset><div><label><input type="radio" name="radio" value="1" class="radio" /> Mínima</label><label><input type="radio" name="radio" value="2" class="radio" /> Máxima</label><label><input type="radio" name="radio" value="3" class="radio" /> Otra: <input type="text" class="field" size="30" style="width:100px;text-align:right;" /> €</label></div><div style="margin-top:1.1em"><label><input type="checkbox" name="checkbox" value="1" checked="checked" class="required" /> Revalorización automática de la base de cotización</label></div></fieldset>';
    FancyConfirm('Base de cotización para el próximo año.', text);
}

function BajaAutonomo() {
    var text = '<p>Con el cese de actividad como autónomo, automáticamente se realizará la baja en el Impuesto de Actividades Económicas. No podrás realizar ninguna actividad económica a partir de éste momento.</p><p><label><input type="checkbox" name="checkbox" value="1" class="required" /> Si, doy mi conformidad para causar la baja como autónomo.</label></p>';
    FancyConfirm('¿Seguro de que quieres causar baja como autónomo?.', text);
}

function AnadirCuentaOTarjeta() {
    var text = '<fieldset><div><label><input type="radio" name="radio" value="1" class="radio" checked="checked" id="radioCuenta" onchange="ChangeCuentaTarjeta();" /> Cuenta bancaria</label><label><input onchange="ChangeCuentaTarjeta();" type="radio" name="radio" value="2" class="radio" /> Tarjeta bancaria</label></div><div id="divBank" style="margin-top:1.1em"><label>Número de cuenta</label><input type="text" name="name" size="30" class="field required" /><label>Para verificar que la cuenta es tuya, se te harán dos pequeños ingresos. Cuando los recibas te pediremos que nos digas el importe exacto que has recibido.</label></div><div id="divCard" style="display:none;margin-top:1.1em"><label>Nombre del titular</label><input type="text" name="name" size="30" class="field required" /><label>Número de tarjeta</label><input type="text" name="name" size="30" class="field required" /><label>Fecha de caducidad</label><input type="date" name="name" size="30" class="field" style="width:120px;" /><label>Código CVV</label><input type="text" name="name" size="30" class="field required" style="width:60px;" /></div></fieldset>';
    FancyConfirm('Añadir una nueva cuenta bancaria o tarjeta.', text);
}
function ChangeCuentaTarjeta() {
    if ($("#radioCuenta").is(':checked')) {
        $("#divBank").show(); 
        $("#divCard").hide();
    }
    else {
        $("#divBank").hide();
        $("#divCard").show();
    }
}

function ModificarEmail() {
    var text = '<fieldset><div><label>Nueva dirección</label><input type="text" name="email" id="email" size="30" class="field required email" placeholder="antoniogarciarubio@gmail.com"></div><div style="margin-top:1.1em"><label>Te enviaremos un email para verificar la cuenta es tuya.</label></div></fieldset>';
    FancyConfirm('Cambiar mi dirección de email.', text);
}

function ModificarTelefono() {
    var text = '<fieldset><div><label>Nuevo número</label><input type="text" name="email" id="email" size="30" class="field required" placeholder="+34 647030087" style="width:12em;"></div><div style="margin-top:1.1em"><label>Te enviaremos un SMS con un código para verificar que es tuyo.</label></div></fieldset>';
    FancyConfirm('Cambiar mi número de teléfono.', text);
}

function PedirDNIPasaporte(isDni) {
    var textAux = (isDni ? 'DNI' : 'pasaporte');
    var textAux2 = (isDni ? 'pasaporte' : 'DNI');
    //var text = '<fieldset><div><label>Oficina de expedición más cercana a tí</label><iframe width="520" height="280" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.es/maps?f=q&amp;source=s_q&amp;hl=es&amp;geocode=+&amp;q=dni+pasaporte+Polic%C3%ADa+Nacional&amp;ie=UTF8&amp;hq=dni+pasaporte+Polic%C3%ADa+Nacional&amp;hnear=Las+Rozas+de+Madrid,+Madrid,+Comunidad+de+Madrid&amp;t=m&amp;fll=40.463536,-3.796635&amp;fspn=0.112972,0.264187&amp;st=116606095092076519610&amp;rq=1&amp;ev=zi&amp;split=1&amp;ll=40.443104,-3.813455&amp;spn=0.112995,0.264187&amp;z=13&amp;iwloc=A&amp;cid=11088585571013750740&amp;output=embed"></iframe><!--select class="select" style="width:100%;"><option></option></select --></div><div style="margin-top:1.1em"><label>Fecha y hora que deseas acudir</label><input type="date" class="field required" style="width:120px;" size="10"/><input type="text" class="field required" style="width:30px;margin-left:10px;" size="10" placeholder="12"/> : <input type="text" class="field required" style="width:30px;" size="10" placeholder="00"/></div><div style="margin-top:1.1em"><label><input type="checkbox" name="checkbox" value="1" /> Aprovechar la cita para renovar también mi ' + textAux2 + '.</label></div></fieldset>';
    var text = '<fieldset><div><label>Selecciona la oficina que mejor te venga para recogerlo</label><table style="width:100%;"><tr><td style="width:50%;" valign="top"><label><input type="radio" name="radio" value="1" class="radio"><b>Pozuelo de Alarcón</b><br /><span style="display:block;padding-left:1.2em">Camino de las Huertas, 36 28224 - Pozuelo de Alarcón (Madrid) <a target="_blank" href="https://maps.google.es/maps?q=dni+pasaporte+comisar%C3%ADa+pozuelo&hl=es&ie=UTF8&sll=40.363445,-3.62198&sspn=0.452559,1.056747&hq=dni+pasaporte+comisar%C3%ADa&hnear=Pozuelo,+Madrid,+Comunidad+de+Madrid&t=m&z=11&iwloc=J">mapa</a><br />917 99 04 82</span></label></td><td style="width:50%;" valign="top"><label><input type="radio" name="radio" value="1" class="radio"><b>Alcorcón</b><br /><span style="display:block;padding-left:1.2em">C/ Alfredo Nobel, 10 28922 - Alcorcón (Madrid) <a target="_blank" href="https://maps.google.es/maps?q=dni+pasaporte+Polic%C3%ADa+Nacional++C%2F+Santa+Engracia,+18++++28010+Madrid&ie=UTF8&hq=dni+pasaporte+Polic%C3%ADa+Nacional&hnear=Calle+de+Santa+Engracia,+18,+28010+Madrid,+Comunidad+de+Madrid&t=m&z=11&iwloc=C">mapa</a><br />914 86 15 92</span></label></td></tr><tr><td style="width:50%;" valign="top"><label><input type="radio" name="radio" value="1" class="radio"><b>Madrid - La Latina</b><br /><span style="display:block;padding-left:1.2em">Avda. de los Poblados s/n 28047 - Madrid (Madrid) <a target="_blank" href="https://maps.google.es/maps?q=dni+pasaporte+comisar%C3%ADa+pozuelo&hl=es&ie=UTF8&ll=40.391012,-3.600082&spn=0.452373,1.056747&sll=40.363445,-3.62198&sspn=0.452559,1.056747&hq=dni+pasaporte+comisar%C3%ADa&hnear=Pozuelo,+Madrid,+Comunidad+de+Madrid&t=m&z=11&iwloc=I">mapa</a><br />913 22 86 60</span></label></td><td style="width:50%;" valign="top"><label><input type="radio" name="radio" value="1" class="radio"><b>Madrid - Arganzuela-Mediodía</b><br /><span style="display:block;padding-left:1.2em">Paseo de la Virgen del Puerto, 47 28005 Madrid <a target="_blank" href="https://maps.google.es/maps?q=dni+pasaporte+comisar%C3%ADa+pozuelo&hl=es&ie=UTF8&ll=40.391012,-3.600082&spn=0.452373,1.056747&sll=40.363445,-3.62198&sspn=0.452559,1.056747&hq=dni+pasaporte+comisar%C3%ADa&hnear=Pozuelo,+Madrid,+Comunidad+de+Madrid&t=m&z=11&iwloc=C">mapa</a><br />913 65 20 11</span></label></td></tr></table></div><div style="margin-top:1.1em"><label>Fecha y hora que deseas acudir</label><input type="date" class="field required" style="width:120px;" size="10"/><input type="text" class="field required" style="width:30px;margin-left:10px;" size="10" placeholder="12"/> : <input type="text" class="field required" style="width:30px;" size="10" placeholder="00"/></div><div style="margin-top:1.1em"><label><input type="checkbox" name="checkbox" value="1" /> Aprovechar la cita para obtener también un nuevo ' + textAux2 + '.</label></div></fieldset>';
    FancyConfirm('Cita previa para solicitar un nuevo.' + textAux + '.', text);
}

function ModificarDirección() {
    var text = '<fieldset><div><label>Nueva dirección</label><input type="text" name="dir" id="dir" size="30" class="field required email" placeholder="C/Castillo de Fuensaldaña 4 - 211"></div><div><label>Código Postal:</label><input type="text" name="cp" id="cp" size="5" class="field required" placeholder="28290" style="width:120px;"></div></fieldset>';
    FancyConfirm('Cambiar mi domicilio postal.', text);
}

function PedirCarnetNuevo() {
    var text = '<fieldset><div><label>Dirección donde deseas recibir tu carnet:</label><input type="text" name="dir" id="dir" size="30" class="field required email" placeholder="Calle, número..." value="C/Castillo de Fuensaldaña 4 - 211"></div><div><label>Código Postal:</label><input type="text" name="cp" id="cp" size="5" class="field required" placeholder="Código postal" value="28290" style="width:120px;"></div></fieldset>';
    FancyConfirm('Solicitar un carnet de conducir nuevo.', text);
}

function TransferirVehiculo() {
    var text = '<p>Para transferir el vehículo, deberás primero rellenar el <a href="img/ContratoCompraventaVehiculo.pdf" target="_blank" style="text-decoration:underline;">contrato de compraventa</a>, firmarlo ambas partes y finalmente enviarlo en el siguiente formulario:</p><fieldset><div><label>DNI de la persona a la que lo transfieres:</label><input type="text" name="dni" id="dni" size="5" class="field required" placeholder="DNI" style="width:120px;"></div><div><label>Contrato de compraventa firmado por ambas partes</label><input type="file" class="field required"></div><br /></fieldset>';
    FancyConfirm('Transferir la titularidad del vehículo.', text);
}

function DarDeBajaVehiculo() {
    var text = '<fieldset style="width:450px;"><div><label><input type="radio" name="bajaVehiculo" class="radio"><b>Baja temporal.</b><span style="display:block;padding-left:1.2em">No podrás circular con él, pero podrás darlo de alta de nuevo en cualquier momento (motivos personales o sustración del vehículo).</span></label></div><div><label><input type="radio" name="bajaVehiculo" class="radio"><b>Baja definitiva.</b><span style="display:block;padding-left:1.2em">No podrás volver a circular con él ni tampoco volver a darlo de alta (envío del vehículo a un desguace o traslado a otro país).</span></label></div><br /></fieldset>';
    FancyConfirm('Dar de baja el vehículo.', text);
}

function PagarMulta(importe, todas) {
    var text = '<fieldset><div><label>Importe:</label> <b>' + importe + ' €</b></div><div><label>Pagar con:</label><select name="pagarCon" class="select"><option>Cuentra principal</option><option>Cuentra secundaria</option></select></div><br /></fieldset>';
    FancyConfirm(todas? 'Pagar todas las multas.':'Pago de la multa.', text);
}

function RecurrirMulta() {
    var text = '<fieldset><div><label>Explica las razones por las qué consideras que no debes pagar la multa</label><textarea name="message" id="message" cols="30" rows="10" class="area" placeholder="Argumenta de forma razonable y detallada, aporta el máximo de información."></textarea></div><div><label>Adjunta las imágenes o archivos que puedan aportar información.</label><input type="file" class="field required"></div><br /></fieldset>';
    FancyConfirm('Recurrir la multa.', text);
}

function IdentificarAlConductor() {
    var text = '<fieldset><div><label>Nombre y apellidos:</label><input type="text" name="nombre" id="nombre" size="30" class="field required" placeholder=""></div><div><label>DNI:</label><input type="text" name="dni" id="dni" size="30" class="field required" placeholder="" style="width:120px"></div><br /></fieldset>';
    FancyConfirm('Identifica a la persona que conducía el vehículo.', text);
}