﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="SentidoComun.index" %>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Ministerio del Sentido Común.</title>	
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="Jesús Barrio">
    <meta name="keywords" content="administración pública, opendata, ministerio del sentido común" />
	<meta name="description" content="Una idea sobre cómo deberían de ser las gestiones con la administración pública." />

    <!-- Le styles -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.css" rel="stylesheet">
    <link href="css/docs.css" rel="stylesheet">

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements d-->
    <!--[if lt IE 9]> 
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="ico/favicon.ico?v=1.0">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="ico/apple-touch-icon-57-precomposed.png">

    <!-- analytics -->
    <script type="text/javascript">
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-386914-28']);
        _gaq.push(['_trackPageview']);

        (function () {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
    </script>

  </head>

  <body data-spy="scroll" data-target=".bs-docs-sidebar">

    <!-- Navbar
    ================================================== -->
    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="brand" href="./index.aspx">Ministerio del Sentido Común</a>
          <div class="nav-collapse collapse">
            <ul class="nav">
              <li class="active">
                <a href="./index.aspx">Inicio</a>
              </li>
              <!--li class="">
                <a href="./getting-started.html">Get started</a>
              </li>
              <li class="">
                <a href="./scaffolding.html">Scaffolding</a>
              </li>
              <li class="">
                <a href="./base-css.html">Base CSS</a>
              </li>
              <li class="">
                <a href="./components.html">Components</a>
              </li>
              <li class="">
                <a href="./javascript.html">Javascript</a>
              </li>
              <li class="">
                <a href="./customize.html">Customize</a>
              </li-->
            </ul>
          </div>
        </div>
      </div>
    </div>

<div class="jumbotron masthead">
  <div class="container">
    <img alt="Ministerio del Sentido Común" src="img/logo.png" />
    <h1>Ministerio del Sentido Común</h1>
    <p>Una idea sobre cómo deberían de ser las gestiones del ciudadano con la administración pública.</p>
    <p><a href="usuarios/index.aspx" class="btn btn-primary btn-large" >Entrar en el ministerio</a></p>
    <ul class="masthead-links">
      <li><a href="https://bitbucket.org/techjb/ministerio-del-sentido-com-n/" >Proyecto en bitbucket</a></li>
      <li>Version 2.0.1</li>
    </ul>
  </div>
</div>

<div class="bs-docs-social">
  <div class="container">
    <ul class="bs-docs-social-buttons">
      <li>
        <iframe class="github-btn" src="http://ghbtns.com/github-btn.html?user=twitter&repo=bootstrap&type=watch&count=true" allowtransparency="true" frameborder="0" scrolling="0" width="100px" height="20px"></iframe>
      </li>
      <li>
        <iframe class="github-btn" src="http://ghbtns.com/github-btn.html?user=twitter&repo=bootstrap&type=fork&count=true" allowtransparency="true" frameborder="0" scrolling="0" width="98px" height="20px"></iframe>
      </li>
      <li class="follow-btn">
        <a href="https://twitter.com/techjb" class="twitter-follow-button" data-link-color="#0069D6" data-show-count="true">Seguir a @techjb</a>
      </li>
      <li class="tweet-btn">
        <a href="https://twitter.com/share" class="twitter-share-button" data-url="http://twitter.github.com/bootstrap/" data-count="horizontal" data-via="twbootstrap" data-related="mdo:Creator of Twitter Bootstrap">Tweet</a>
      </li>
    </ul>
  </div>
</div>

<div class="container">
  <div class="marketing">
    <h1>Una administración más eficiente es posible.</h1>
    <p class="marketing-byline">Una idea de los ciudadanos para los ciudadanos.</p>

    <div class="row-fluid">
      <div class="span4">
        <img src="img/bs-docs-twitter-github.png">
        <h2>Centralizada.</h2>
        <p>Porque no necesitas <a href="https://sede.060.gob.es/directorio_sedes.html">72 sedes electrónicas</a> para comunicarte con la administración, mejor un solo lugar donde realizar todas las gestiones con la administración local, provincial y nacional.</p>
      </div>
      <div class="span4">
        <img src="img/bs-docs-responsive-illustrations.png">
        <h2>Accesible.</h2>
        <p>Porque la adminstración pública debe hablar con lenguaje natural que entienda todo el mundo, simplificar al máximo los procedimientos y ser accesible desde todos de los dispositivos.</p>
      </div>
      <div class="span4">
        <img src="img/bs-docs-bootstrap-features.png">
        <h2>Inteligente.</h2>
        <p>Porque la administración no debe hacerte perder el tiempo preguntandote el lugar y la fecha de nacimiento, si cobras pensión de viudedad o si tienes estudios superiores porque ya lo sabe.</p>
      </div>
    </div>

    <!--hr class="soften">

    <h1>Built with Bootstrap.</h1>
    <p class="marketing-byline">For even more sites built with Bootstrap, <a href="http://builtwithbootstrap.tumblr.com/" target="_blank">visit the unofficial Tumblr</a> or <a href="./getting-started.html#examples">browse the examples</a>.</p>
    <div class="row-fluid">
      <ul class="thumbnails example-sites">
        <li class="span3">
          <a class="thumbnail" href="http://soundready.fm/" target="_blank">
            <img src="img/example-sites/soundready.png" alt="SoundReady.fm">
          </a>
        </li>
        <li class="span3">
          <a class="thumbnail" href="http://kippt.com/" target="_blank">
            <img src="img/example-sites/kippt.png" alt="Kippt">
          </a>
        </li>
        <li class="span3">
          <a class="thumbnail" href="http://www.fleetio.com/" target="_blank">
            <img src="img/example-sites/fleetio.png" alt="Fleetio">
          </a>
        </li>
        <li class="span3">
          <a class="thumbnail" href="http://www.jshint.com/" target="_blank">
            <img src="img/example-sites/jshint.png" alt="JS Hint">
          </a>
        </li>
      </ul>
     </div>
     -->
  </div>

</div>



    <!-- Footer
    ================================================== -->
    <footer class="footer">
      <div class="container">
        <p class="pull-right"><a href="#">Volver al inicio</a></p>
        <p>Desarrollado por <a href="https://twitter.com/techjb" target="_blank">@techjb</a> con el framework <a href="http://twitter.github.com/bootstrap/" target="_blank">Bootstrap</a>.</p>
        <p>Código con licencia <a href="http://www.apache.org/licenses/LICENSE-2.0" target="_blank">Apache License v2.0</a>. Documentación de la licencia en <a href="http://creativecommons.org/licenses/by/3.0/">CC BY 3.0</a>.</p>
        <p>Iconos de <a href="http://glyphicons.com">Glyphicons Free</a> y  <a href="http://thenounproject.com/">The Noun Project</a>, licencia <a href="http://creativecommons.org/licenses/by/3.0/">CC BY 3.0</a>.</p>
        <!--
            Paleta de colores: http://www.colorcombos.com/combomaker.html?design=cupcakes&output_width=100&size_option=element&colors=6600CC,FFCC00,000000,CC0000&background_color=FFFFFF&show_hex_flag=Y
        -->
        <!--ul class="footer-links">
          <li><a href="http://blog.getbootstrap.com">Read the blog</a></li>
          <li><a href="https://github.com/twitter/bootstrap/issues?state=open">Submit issues</a></li>
          <li><a href="https://github.com/twitter/bootstrap/wiki">Roadmap and changelog</a></li>
        </ul-->
      </div>
    </footer>



    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>
