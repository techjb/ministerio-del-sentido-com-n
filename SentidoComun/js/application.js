﻿$(function () {
    var $window = $(window);
    $('.div-nav-list').affix({
        offset: {
            top: function () { return $window.width() <= 980 ? 999999 : 110 },
            bottom: 250
        }
    })
})



function MPrompt(title, text, isAlert) {
    isAlert = isAlert || false;
    OpenModal(title, '<p>' + text + '</p>', isAlert);
}

function OpenModal(title, text, isAlert) {
    isAlert = isAlert || false;
    $('#H1').html(title);
    $('#modal-body').html(text);
    $('#modalWindow').modal();
    if (isAlert) {
        $('#btnModalCerrar').show();
        $('#btnModalCancelar').hide();
        $('#btnModalAceptar').hide();
        
    }
    else {
        $('#btnModalCerrar').hide();
        $('#btnModalCancelar').show();
        $('#btnModalAceptar').show();
    }
}
/*****************************************************************/
function EsSoloUnaIdea() {
    OpenModal('Esto es solo una idea', 'Pero si te ha gustado', true);
}
function BorrarCuentaBancariaPrincipal() {
    MPrompt("¿Quieres borrar tu cuenta principal?", "La cuenta secundaria pasará a funcionar como cuenta principal.");
}

function BorrarCuentaSecundaria() {
    MPrompt("¿Quieres borrar tu cuenta secundaria?", "Todos los ingresos y domiciliaciones asociados a esta cuenta, pasarán a asociarse con la cuenta principal.");
}

function BorrarTarjeta() {
    MPrompt("¿Quieres borrar la tarjeta?", "Los cobros que tengas asociados con esta tarjeta, pasarán a realizarse con tu cuenta bancaria principal.");
}

function CambiarEmail() {
    var text = '<p><label>Nueva dirección de email</label> <input class="input-xlarge" type="text" placeholder="antoniogarciarubio@gmail.com"></p><p>Te enviaremos un email para verificar la cuenta es tuya.</p>'
    OpenModal('Cambiar mi dirección de email',text);
}

function CambiarTelefonoMovil() {
    var text = '<p><label>Nuevo número de móvil</label> <input class="input-medium" type="text" placeholder="+34 647030087"></p><p>Te enviaremos un SMS con un código para verificar que es tuyo.</p>'
    OpenModal('Cambiar mi teléfono móvil', text);
}

function IntroducirCodigoVerificacionDirecionPostal() {
    var text = '<p><label>Código de verificación que se ha enviado a tu nueva dirección postal</label> <input class="input-small" type="text" placeholder=""></p>';
    OpenModal('Verificar dirección postal', text);
}

function MostrarCentrosItv() {
    MostrarPopupMapa('Centros de ITV cercanos a tí', 'https://maps.google.es/maps/ms?ie=UTF8&hl=es&msa=0&msid=215203341414301936863.0004800afba4167cbd2eb&source=embed&ll=40.565981,-3.732605&spn=0.834583,1.565552&t=m&output=embed')
    
}

function MostrarPopupMapa(titulo, url) {
    OpenModal(titulo, '<iframe style="width:100%;height:380px;" frameborder="0" src="' + url + '"></iframe>', true);
}
