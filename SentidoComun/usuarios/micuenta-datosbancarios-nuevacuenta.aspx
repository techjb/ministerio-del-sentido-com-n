﻿<%@ Page Language="C#" MasterPageFile="Usuarios.Master" AutoEventWireup="true" CodeBehind="micuenta-datosbancarios-nuevacuenta.aspx.cs" Inherits="SentidoComun.usuarios.micuenta_datosbancarios_nuevacuenta" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server" >
            <ul class="breadcrumb">
                <li><a href="micuenta-datosbancarios.aspx">Datos bancarios</a> <span class="divider">/</span></li>
                <li class="active">Nueva cuenta</li>
            </ul>
			<div class="page-header">
				<h3>Nueva cuenta bancaria o tarjeta</h3>
			</div>
            
                <ul id="myTab" class="nav nav-tabs">
                  <li class="active"><a href="#cuenta" data-toggle="tab">Cuenta bancaria</a></li>
                  <li class=""><a href="#tarjeta" data-toggle="tab">Tarjeta</a></li>
                </ul>
                <div id="myTabContent" class="tab-content">
                  <div class="tab-pane fade active in" id="cuenta">
                    <form class="well">
                        <p> 
                            <label>Nombre abreviado para recordarlo</label>
                            <input type="text" placeholder="Ejemplo: Cuenta del BBV" class="input-xlarge">
                            <label>Número de cuenta</label>
                            <input type="text" placeholder="Número de veinte dígitos" class="input-xlarge">
                            <div class="alert alert-info">
                                Para verificar que la cuenta es tuya, se te hará un pequeño ingreso inferior a 1 € que después deberás de confirmar.
                            </div>
                        </p>
                        <p>
                         <button type="submit" class="btn btn-primary">Continuar</button>
                         </p>
                    </form>
                  </div>
                  <div class="tab-pane fade" id="tarjeta">
                    <form class="well">
                        <p> 
                            <label>Nombre abreviado para recordarlo</label>
                            <input type="text" placeholder="Ejemplo: Tarjeta visa Santander" class="input-xlarge">
                            <label>Titular de la tarjeta</label>
                            <input type="text" placeholder="Nombre tal y como aparece en la tarjeta" class="input-xlarge">
                            <label>Número de tarjeta</label>
                            <input type="text" placeholder="Número de dieciséis dígitos" class="input-xlarge">
                            <label>Fecha de caducidad</label>
                            <input type="text" placeholder="mm/yy" class="input-mini">
                            <label>Código CVV</label>
                            <input type="text" placeholder="" class="input-mini">
                            <div class="alert alert-info">
                                Para verificar que la tarjeta es tuya, se realizará un cargo de 0,01 € y a continuación un ingreso de 0,01 €.
                            </div>
                            <p>
                             <button type="submit" class="btn btn-primary">Continuar</button>
                             </p>
                        </p>
                    </form>
                  </div>
                </div>
                <br />
                
            
			
</asp:Content>