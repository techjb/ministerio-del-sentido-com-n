﻿<%@ Page Language="C#" MasterPageFile="Usuarios.Master" AutoEventWireup="true" CodeBehind="micuenta-datosbancarios.aspx.cs" Inherits="SentidoComun.usuarios.micuenta_datosbancarios" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server" >
           
			<div class="page-header">
				<h1>Tus datos bancarios</h1>
			</div>
            <table class="table">
                <tbody>
                    <tr><td><strong>Cuenta principal:</strong></td><td>2100 5801 95 02 00096756 <span><i class="icon-ok"></i> <strong>Vefiricada</strong></span> <a href="javascript:BorrarCuentaBancariaPrincipal();">Eliminar</a></td></tr>
                    <tr><td><strong>Cuenta secundaria:</strong></td><td>0227 0003 76 02 06152213 <span><i class="icon-ok"></i> <strong>Vefiricada</strong></span> <a href="javascript:BorrarCuentaSecundaria();">Eliminar</a></td></tr>
                    <tr><td><strong>Tarjeta:</strong></td><td>XXXX XXXX XXXX XXXX 6873  <span><i class="icon-time"></i> <strong>Pendiente de verificar</strong></span> <a href="javascript:BorrarTarjeta();">Eliminar</a></td></tr>
                </tbody>
            </table>
            <p>
                <a class="btn" href="micuenta-datosbancarios-nuevacuenta.aspx"><i class=" icon-plus"></i> Añadir una nueva cuenta o tarjeta</a>
            </p>
            <h3>Pagos y domiciliaciones</h3>
            <table class="table table-striped table-condensed">
                <tbody>
                    <tr><td></td><td><h5>Pagar con:</h5></td></tr>
                    <tr><td>Cotización de la seguridad social:</td><td><select><option>Cuenta principal</option><option>Cuenta secundaria</option></select></td></tr>
                    <tr><td>Multas de tráfico (pasado el periodo para recurrir):</td><td><select><option>Cuenta principal</option><option>Cuenta secundaria</option></select></td></tr>
                    <tr><td>Impuesto sobre la renta:</td><td><select><option>Cuenta principal</option><option>Cuenta secundaria</option></select></td></tr>
                    <tr><td>Impuesto sobre bienes inmuebles:</td><td><select><option>Cuenta principal</option><option>Cuenta secundaria</option></select></td></tr>
                    <tr><td>Impuesto sobre vehículos:</td><td><select><option>Cuenta principal</option><option selected="selected">Cuenta secundaria</option></select></td></tr>
                    <tr><td>Impuesto de actividades económicas:</td><td><select><option>Cuenta principal</option><option>Cuenta secundaria</option></select></td></tr>
                     <tr><td></td><td><h5>Ingresar en:</h5></td></tr>
                    <tr><td>Prestaciones por desempleo:</td><td><select><option>Cuenta principal</option><option>Cuenta secundaria</option></select></td></tr>
                    <tr><td>Resultado de impuestos a devolver:</td><td><select><option>Cuenta principal</option><option  selected="selected">Cuenta secundaria</option></select></td></tr>
                    <tr><td>Subvenciones:</td><td><select><option>Cuenta principal</option><option>Cuenta secundaria</option></select></td></tr>
                </tbody>
            </table>
			
</asp:Content>