﻿<%@ Page Language="C#" MasterPageFile="Usuarios.Master"  AutoEventWireup="true" CodeBehind="micuenta-datospersonales-cambiodomicilio.aspx.cs" Inherits="SentidoComun.usuarios.micuenta_datospersonales_cambiodomicilio" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server" >
            <ul class="breadcrumb">
                <li><a href="micuenta-datospersonales.aspx">Datos personales</a> <span class="divider">/</span></li>
                <li class="active">Cambio de domicilio</li>
            </ul>
			<div class="page-header">
				<h3>Cambio de domicilio</h3>
			</div>
            <form class="well">
                <legend>Nuevo domicilio</legend>
                <p>
                    <label>Calle</label>
                    <input type="text" placeholder="" class="input-xlarge" >
                    <label>Número</label>
                    <input type="text" placeholder="" class="input-mini" >
                    <label>Bloque piso y letra</label>
                    <input type="text" placeholder="bloque" class="span1" >
                    <input type="text" placeholder="piso" class="span1" >
                    <input type="text" placeholder="letra" class="span1" >
                    <label>Código postal</label>
                    <input type="text" placeholder="" class="input-mini" >
                </p>
                <br />
                <p>
                    <button type="submit" class="btn btn-primary">Continuar</button>
                </p>
            </form>
			
</asp:Content>