﻿<%@ Page Language="C#" MasterPageFile="Usuarios.Master"  AutoEventWireup="true" CodeBehind="micuenta-datospersonales-nuevopasaporte.aspx.cs" Inherits="SentidoComun.usuarios.micuenta_datospersonales_nuevopasaporte" %>
<%@ Register src="micuenta-datospersonales-oficinasdnipasaporte.ascx" tagname="micuenta" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server" >
           
            <ul class="breadcrumb">
                <li><a href="micuenta-datospersonales.aspx">Datos personales</a> <span class="divider">/</span></li>
                <li class="active">Solicitar un nuevo Pasaporte</li>
            </ul>

			<div class="page-header">
				<h3>Soliciar nuevo Pasaporte</h3>
			</div>
            <p class="alert alert-info">
                Para solicitar un nuevo Pasaporte deberás pedir una cita previa en una oficina de expedición. Selecciona la que te quede más cercana:
            </p>
             <form id="form1" runat="server" class="well">
                <uc1:micuenta ID="micuenta1" runat="server" />
                
                <p>
                    <label class="checkbox"> <input type="checkbox"> Aprovechar la cita para obtener también un nuevo DNI.
                </p>
                <p>
                    <button type="submit" class="btn btn-primary">Continuar</button>
                </p>
            </form>
			
</asp:Content>