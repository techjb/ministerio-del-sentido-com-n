﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="micuenta-datospersonales-oficinasdnipasaporte.ascx.cs" Inherits="SentidoComun.usuarios.micuenta_datospersonales_oficinasdnipasaporte" %>
                <p>
                    <label  class="radio">
                         <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
                            <strong>Pozuelo de Alarcón</strong><br />
                            Camino de las Huertas, 36. 28224 - Pozuelo de Alarcón (Madrid) <a target="_blank" href="https://maps.google.es/maps?q=dni+pasaporte+comisar%C3%ADa+pozuelo&hl=es&ie=UTF8&sll=40.363445,-3.62198&sspn=0.452559,1.056747&hq=dni+pasaporte+comisar%C3%ADa&hnear=Pozuelo,+Madrid,+Comunidad+de+Madrid&t=m&z=11&iwloc=J"><i class="icon-map-marker"></i> Mapa</a><br />
                            917 99 04 82
                    </label>
                </p>
                <p>
                    <label  class="radio">
                         <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
                            <strong>Alcorcón</strong><br />
                            C/ Alfredo Nobel, 10. 28922 - Alcorcón (Madrid) <a target="_blank" href="https://maps.google.es/maps?q=dni+pasaporte+Polic%C3%ADa+Nacional++C%2F+Santa+Engracia,+18++++28010+Madrid&ie=UTF8&hq=dni+pasaporte+Polic%C3%ADa+Nacional&hnear=Calle+de+Santa+Engracia,+18,+28010+Madrid,+Comunidad+de+Madrid&t=m&z=11&iwloc=C"><i class="icon-map-marker"></i> Mapa</a><br />
                            914 86 15 92
                    </label>
                </p>
                 <p>
                    <label  class="radio">
                         <input type="radio" name="optionsRadios" id="optionsRadios3" value="option3">
                            <strong>Madrid - La Latina</strong><br />
                            Avda. de los Poblados s/n 28047 - Madrid (Madrid)  <a target="_blank" href="https://maps.google.es/maps?q=dni+pasaporte+comisar%C3%ADa+pozuelo&hl=es&ie=UTF8&ll=40.391012,-3.600082&spn=0.452373,1.056747&sll=40.363445,-3.62198&sspn=0.452559,1.056747&hq=dni+pasaporte+comisar%C3%ADa&hnear=Pozuelo,+Madrid,+Comunidad+de+Madrid&t=m&z=11&iwloc=I"><i class="icon-map-marker"></i> Mapa</a><br />
                            913 22 86 60
                    </label>
                </p>
                <p>
                    <label  class="radio">
                         <input type="radio" name="optionsRadios" id="Radio1" value="option3">
                            <strong>Madrid - Arganzuela-Mediodía</strong><br />
                            Paseo de la Virgen del Puerto, 47 28005 Madrid <a target="_blank" href="https://maps.google.es/maps?q=dni+pasaporte+comisar%C3%ADa+pozuelo&hl=es&ie=UTF8&ll=40.391012,-3.600082&spn=0.452373,1.056747&sll=40.363445,-3.62198&sspn=0.452559,1.056747&hq=dni+pasaporte+comisar%C3%ADa&hnear=Pozuelo,+Madrid,+Comunidad+de+Madrid&t=m&z=11&iwloc=C"><i class="icon-map-marker"></i> Mapa</a><br />
                            913 65 20 11
                    </label>
                </p>
                <p>
                    <label>Fecha y hora que deseas acudir</label>
                    <input type="date" placeholder="dd/mm/aa" class="span2" >
                    <input type="text" placeholder="00:00" class="span1" >
                </p>
                