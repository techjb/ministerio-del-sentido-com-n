﻿<%@ Page Language="C#" MasterPageFile="Usuarios.Master"   AutoEventWireup="true" CodeBehind="micuenta-datospersonales.aspx.cs" Inherits="SentidoComun.usuarios.micuenta_datospersonales" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server" >
            
			<div class="page-header">
				<h1>Tus datos personales</h1>
			</div>
            <table class="table">
                <tbody>
                  <tr><td><strong>Fotografía:</strong></td><td><img src="../img/fotocarnetgrande.jpg" class="img-polaroid"></td></tr>
                  <tr><td><strong>Nombre</strong>:</td><td>Antonio</td></tr>
                  <tr><td><strong>Apellidos:</strong></td><td>García Rubio</td></tr>
                  <tr><td><strong>Nacimiento:</strong></td><td>15/07/1980 en Madrid (España)</td></tr>
                  <tr><td><strong>Hijo de:</strong></td><td>Fernando y Maria Jose.</td></tr>
                  <tr><td><strong>DNI:</strong></td><td>50925433J<br /> Expedido el 3/4/2010. Válido hasta el 3/4/2015.<br />¿Lo has perdido? <a href="micuenta-datospersonales-nuevodni.aspx">Pedir un DNI nuevo</a>.</td></tr>
                  <tr><td><strong>Pasaporte:</strong></td><td>AAD1258865<br /> Expedido el 8/2/2011. Válido hasta el 8/2/2016. <br />¿Lo has perdido? <a href="micuenta-datospersonales-nuevopasaporte.aspx" >Pedir pasaporte nuevo</a>.</td></tr>
                  <tr><td><strong>Domicilio:</strong></td><td>C/Castillo de Belmonte 8 - 3ºA, 28232, Las Rozas de Madrid (Madrid).<br /><a href="micuenta-datospersonales-cambiodomicilio.aspx">Pedir un cambio de domicilio</a>.</td></tr>
                </tbody>
            </table>
            <p>
            </p>
</asp:Content>