﻿<%@ Page Language="C#" MasterPageFile="Usuarios.Master" AutoEventWireup="true" CodeBehind="micuenta-notificaciones.aspx.cs" Inherits="SentidoComun.usuarios.micuenta_notificaciones" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server" >
            <script type="text/javascript">
                $(function () {
                    $('#linkVerificarDireccion').tooltip();
                });
            </script>
           
			<div class="page-header">
				<h1>Notificaciones</h1>
			</div>
           
            <table class="table">
                <tbody>
                    <tr><td><strong>Email:</strong></td><td>antoniogarciarubio@gmail.com <span><i class="icon-ok"></i> <strong>Vefiricado</strong></span> <a href="javascript:CambiarEmail();">Cambiar</a></td></tr>
                    <tr><td><strong>Teléfono móvil:</strong></td><td>+34 647030087 <span><i class="icon-ok"></i> <strong>Vefiricado</strong></span> <a href="javascript:CambiarTelefonoMovil();">Cambiar</a></td></tr>
                    <tr><td><strong>Dirección postal:</strong></td><td>C/ Castillo de Fuensaldaña 4 - 211, 28290, Las Rozas de Madrid (Madrid)  <span><i class="icon-time"></i> <strong><span id="linkVerificarDireccion" rel="tooltip" data-original-title="Se ha enviado una carta con un código a tu nueva dirección postal. En cuanto la recibas deberás introducir el código de verificación.">Pendiente de verificar</span></strong></span> <a href="javascript:IntroducirCodigoVerificacionDirecionPostal();" >Introducir código de verificación</a></td></tr>
                    
                    
                </tbody>
            </table>
            <h3>Notificarme mediante:</h3>
             <table class="table table-striped table-condensed">
                <tbody>
                    <tr><td>Cargos en tu cuenta bancaria:</td><td><label class="checkbox"><input type="checkbox" checked="checked"> Email</label></td><td><label class="checkbox"><input type="checkbox"> SMS</label></td><td><label class="checkbox"><input type="checkbox"> Carta</label></td></tr>
                    <tr><td>Ingresos en tu cuenta bancaria:</td><td><label class="checkbox"><input type="checkbox" checked="checked"> Email</label></td><td><label class="checkbox"><input type="checkbox"> SMS</label></td><td><label class="checkbox"><input type="checkbox"> Carta</label></td></tr>
                    <tr><td>Multas y requerimientos de tráfico:</td><td><label class="checkbox"><input type="checkbox" checked="checked"> Email</label></td><td><label class="checkbox"><input type="checkbox" checked="checked"> SMS</label></td><td><label class="checkbox"><input type="checkbox"> Carta</label></td></tr>
                    <tr><td>Resto de notificaciones de tráfico:</td><td><label class="checkbox"><input type="checkbox" checked="checked"> Email</label></td><td><label class="checkbox"><input type="checkbox"> SMS</label></td><td><label class="checkbox"><input type="checkbox"> Carta</label></td></tr>
                    <tr><td>Cambios en tu base de cotización de seguridad social:</td><td><label class="checkbox"><input type="checkbox" checked="checked"> Email</label></td><td><label class="checkbox"><input type="checkbox"> SMS</label></td><td><label class="checkbox"><input type="checkbox"> Carta</label></td></tr>
                    <tr><td>Denuncias recibidas:</td><td><label class="checkbox"><input type="checkbox" checked="checked"> Email</label></td><td><label class="checkbox"><input type="checkbox"> SMS</label></td><td><label class="checkbox"><input type="checkbox"> Carta</label></td></tr>
                    <tr><td>Citaciones con la administración:</td><td><label class="checkbox"><input type="checkbox" checked="checked"> Email</label></td><td><label class="checkbox"><input type="checkbox" checked="checked"> SMS</label></td><td><label class="checkbox"><input type="checkbox"> Carta</label></td></tr>
                    
                    
                </tbody>
            </table>

			

</asp:Content>