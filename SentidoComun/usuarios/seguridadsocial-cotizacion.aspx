﻿<%@ Page Language="C#" MasterPageFile="Usuarios.Master" AutoEventWireup="true" CodeBehind="seguridadsocial-cotizacion.aspx.cs" Inherits="SentidoComun.usuarios.seguridadsocial_cotizacion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server" >
        <div class="row-fluid">
			<div class="page-header">
				<h1>Cotización</h1>
			</div>
            <p class="lead">
               Base actual de cotización: <strong>850,20 €</strong>.
            </p>
             <h2>Historial de cotización</h2>
            <p>
                Has cotizado durante 2934 días un total de 18.756,72 €.
            </p>
            
            <div id="placeholder" style="display:block;height:300px;margin:20px 0;width:99%;"></div>
            
            <script src="../js/jquery.js"></script>            
            <script type="text/javascript">
                $(function () {

                    $.getScript("../js/jquery.flot.js", function () {
                        $.getScript("../js/jquery.flot.resize.js", function () {
                            LoadPlot();
                        });
                    });


                });
                function LoadPlot() 
                {
                    var data = [
		            {
		                label: 'Base',
		                data: [[0, 500], [1, 600], [2, 700], [3, 800]]
		            },
                    {
                        label: 'Cotización',
                        data: [[0, 200], [1,300], [2, 350], [3, 365]]
                    },
                    {
                        label: 'Recargo',
                        data: [[0, 0], [1, 0], [2, 50], [3, 90]]
                    }
                    ];

                    var options = {
                        legend: {
                            show: true,
                            margin: 10,
                            backgroundOpacity: 0.5
                        },
                        points: {
                            show: true,
                            radius: 3
                        },
                        lines: {
                            show: true
                        },
                        grid: {
                            borderWidth: 1,
                            hoverable: true
                        },
                        xaxis: {
                            axisLabel: 'Mes',
                            ticks: [[0, 'Ene'], [1, 'Feb'], [2, 'Mar'], [3, 'Abr'], [4, 'May'], [5, 'Jun'], [6, 'Jul'], [7, 'Ago'], [8, 'Sep'], [9, 'Oct'], [10, 'Nov'], [11, 'Dic']],
                            tickDecimals: 0
                        },
                        yaxis: {
                            tickSize: 100,
                            tickDecimals: 0
                        }
                    };
                   
                    function showTooltip(x, y, contents) {
                        $('<div id="tooltip">' + contents + '</div>').css({
                            position: 'absolute',
                            display: 'none',
                            top: y + 5,
                            left: x + 5,
                            border: '1px solid #D6E9C6',
                            padding: '2px',
                            'background-color': '#DFF0D8',
                            opacity: 0.80
                        }).appendTo("body").fadeIn(200);
                    }
                    var previousPoint = null;
                    $("#placeholder").bind("plothover", function (event, pos, item) {
                        if (item) {
                            if (previousPoint != item.dataIndex) {
                                previousPoint = item.dataIndex;

                                $("#tooltip").remove();
                                showTooltip(item.pageX, item.pageY, item.series.label + ": " + item.datapoint[1] + " €");
                            }
                        }
                        else {
                            $("#tooltip").remove();
                            previousPoint = null;
                        }
                    });
                    $.plot($("#placeholder"), data, options);
                }
            </script>


            

            
            <table class="table table-striped table-bordered table-condensed">
				<thead>
					<tr>
						<th>Régimen</th>
						<th>Empresa</th>
                        <th>Comienzo</th>
                        <th>Fin</th>
						<th>Días</th>
                        <th></th>
					</tr>
				</thead>
				<tbody>
                    <tr><td>Autónomo</td><td>Autónomo: Actividades de ingeniería</td><td>1/10/2009</td><td></td><td><span id="timeSpan3">1105</span></td><td><button type="button" class="btn btn-mini" href="">Rectificar</button></td></tr>
                    <tr><td>General</td><td>Telecom Universe S.L.</td><td>1/09/2005</td><td>30/09/2009</td><td>1490</td><td></td></tr>
                    <tr><td>General</td><td>Implemental Systems S.L.</td><td>28/09/2004</td><td>15/08/2005</td><td>322</td><td></td></tr>
                </tbody>
			</table>
            
			
        </div>
</asp:Content>