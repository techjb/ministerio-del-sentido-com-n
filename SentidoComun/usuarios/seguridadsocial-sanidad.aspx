﻿<%@ Page Language="C#" MasterPageFile="Usuarios.Master" AutoEventWireup="true" CodeBehind="seguridadsocial-sanidad.aspx.cs" Inherits="SentidoComun.usuarios.seguridadsocialsanidad" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server" >
        	<div class="page-header">
				<h1>Sanidad</h1>
			</div>
             
            <div class="well">
                <legend>Tu médico de cabecera</legend>
                <img src="../img/marialuisa.png"  class="img-polaroid" style="float:left;margin:0 20px 20px 0;">
                <div>
                    
                    <h4>Maria Luisa González</h4>
                    <dl>                        
                        <dt>Teléfono</dt>
                        <dd><a href="tel:637050045">637050045</a></dd>
                        <dt>Horario</dt>
                        <dd>Lunes a viernes 9.00 - 15:00</dd>
                        <dt>Centro</dt>
                        <dd>Centro de Salud Las Rozas. 916 37 55 42. <a href="javascript:MostrarPopupMapa('Centro de Salud Las Rozas','https://maps.google.es/maps?q=centro+de+salud+las+rozas&hl=es&sll=40.503554,-3.878517&sspn=0.056452,0.132093&hq=centro+de+salud+las+rozas&t=m&ie=UTF8&hnear=&z=14&iwloc=A&cid=88813944091264347&ll=40.48942,-3.878171&output=embed');" target="_blank"><i class="icon-map-marker"></i> Mapa</a></dd>
                    </dl>
                    <p>
                        <button class="btn btn-primary">Pedir cita con mi médico</button>
                    </p>
                </div>
                
            </div>
            <p>
                <a href="javascript:EsSoloUnaIdea();">Cambiar de médico de cabecera</a>
            </p>
            
            <h3>Tu historial de vacunación</h3>
            <p>Vacunas que te has puesto hasta hoy.</p>
            <table class="table table-striped table-bordered table-condensed">
				<thead>
					<tr>
						<th>Fecha</th>
						<th>Centro</th>
						<th>Vacuna</th>
						<th>Observaciones</th>
					</tr>
				</thead>
				<tbody>
                    <tr><td valign="top">28/07/1994</td><td valign="top">..</td><td valign="top">Tétanos-difteria tipo adulto, Virus Papiloma Humano</td><td valign="top"></td></tr>
                    <tr><td valign="top">20/07/1991</td><td valign="top">..</td><td valign="top">Varicela</td><td valign="top"></td></tr>
                    <tr><td valign="top">25/07/1984</td><td valign="top">..</td><td valign="top">Difteria-Tétanos-Tos ferina acelular, Sarampión-Rubéola-Parotiditis</td><td valign="top"></td></tr>
                    <tr><td valign="top">27/03/1982</td><td valign="top">..</td><td valign="top">Difteria-Tétanos-Tos ferina acelular, Haemophilus Influenzae b, Polio</td><td valign="top"></td></tr>
                    <tr><td valign="top">26/12/1981</td><td valign="top">..</td><td valign="top">Rubéola-Parotiditis-Sarampión, Meningococo C, Varicela</td><td valign="top"></td></tr>
                    <tr><td valign="top">21/03/1981</td><td valign="top">..</td><td valign="top">Hepatitis B, Difteria, Tétanos, Tor ferina, Influenzae b, Polio inactiva</td><td valign="top"></td></tr>
                    <tr><td valign="top">18/01/1981</td><td valign="top">..</td><td valign="top">Hepatitis B, Difteria, Tétanos, Tor ferina, Influenzae b, Polio inactiva, Meningococo c</td><td valign="top"></td></tr>
                    <tr><td valign="top">20/09/1980</td><td valign="top">..</td><td valign="top">Hepatitis B, Difteria, Tétanos, Tor ferina, Influenzae b, Polio inactiva, Meningococo c</td><td valign="top"></td></tr>
                    <tr><td valign="top">20/07/1980</td><td valign="top">..</td><td valign="top">Hepatitis B</td><td valign="top"></td></tr>
                </tbody>
			</table>
            
</asp:Content>