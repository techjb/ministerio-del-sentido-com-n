﻿<%@ Page Language="C#" MasterPageFile="Usuarios.Master" AutoEventWireup="true" CodeBehind="seguridadsocial-trabajo-bajaautonomo.aspx.cs" Inherits="SentidoComun.usuarios.seguridadsocial_trabajo_bajaautonomo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server" >
            <ul class="breadcrumb">
                <li><a href="seguridadsocial-trabajo.aspx">Trabajo</a> <span class="divider">/</span></li>
                <li class="active">Baja de autónomo</li>
            </ul>
			<div class="page-header">
				<h3>Baja de autónomo</h3>
			</div>
            <form class="well">
                <legend>¿Seguro de que quieres causar baja como autónomo?.</legend>
            
                <p>
                   Con el cese de actividad como autónomo, automáticamente se realizará la baja en el Impuesto de Actividades Económicas. No podrás realizar ninguna actividad económica a partir de éste momento.
                </p>
                <p>
                    <label class="checkbox">
                      <input type="checkbox" value="">
                      Si, doy mi conformidad para causar la baja como autónomo.
                    </label>
                </p>
                <br />
                <p>
                    <button type="submit" class="btn btn-danger">Darme de baja</button>
                </p>
            </form>
            
			
</asp:Content>