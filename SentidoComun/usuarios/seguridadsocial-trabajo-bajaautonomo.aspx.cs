﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SentidoComun.usuarios
{
    public partial class seguridadsocial_trabajo_bajaautonomo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ((Usuarios)Master).liTrabajo.Attributes.Add("class", "active");
        }
    }
}