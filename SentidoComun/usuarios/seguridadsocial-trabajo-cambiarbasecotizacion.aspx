﻿<%@ Page Language="C#"  MasterPageFile="Usuarios.Master"  AutoEventWireup="true" CodeBehind="seguridadsocial-trabajo-cambiarbasecotizacion.aspx.cs" Inherits="SentidoComun.usuarios.seguridadsocial_trabajo_cambiarbasecotizacion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server" >
            <ul class="breadcrumb">
                <li><a href="seguridadsocial-trabajo.aspx">Trabajo</a> <span class="divider">/</span></li>
                <li class="active">Cambiar base de cotización</li>
            </ul>
			<div class="page-header">
				<h3>Cambiar base de cotización</h3>
			</div>
            <form class="well">
                <legend>Base de cotización para el próximo año.</legend>
            
                <p>
                   <label class="radio">
                      <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1">
                        Mínima
                    </label>
                    <label class="radio">
                      <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
                      Máxima
                    </label>
                    <label class="radio">
                      <input type="radio" name="optionsRadios" id="optionsRadios3" value="option3">
                      Otra: <input type="text" class="input-small" /> €
                    </label>
                </p>
                <p>
                    <label class="checkbox">
                      <input type="checkbox" value="" checked>
                      Revalorizar automáticamente la base de cotización.
                    </label>
                </p>
                <br />
                <p >
                    <button type="submit" class="btn btn-primary">Continuar</button>
                </p>              
                
            </form>
			
</asp:Content>