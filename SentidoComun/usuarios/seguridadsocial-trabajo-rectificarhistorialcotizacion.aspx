﻿<%@ Page Language="C#" MasterPageFile="Usuarios.Master"  AutoEventWireup="true" CodeBehind="seguridadsocial-trabajo-rectificarhistorialcotizacion.aspx.cs" Inherits="SentidoComun.usuarios.seguridadsocial_trabajo_rectificarhistorialcotizacion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server" >
            <ul class="breadcrumb">
                <li><a href="seguridadsocial-trabajo.aspx">Trabajo</a> <span class="divider">/</span></li>
                <li class="active">Rectificar historial de cotización</li>
            </ul>
			<div class="page-header">
				<h3>Rectificar historial de cotización</h3>
			</div>
            <form class="well">
                <legend><asp:Label ID="LabelEmpresa" runat="server" Text=""></asp:Label></legend>
            
                <p>
                    <label>Periodo de liquidación.</label>
                    <input type="date" class="input-medium" placeholder="">

                    <label>Base de cotización.</label>
                    <input type="text" class="input-small" placeholder=""> €
                    <label>Observaciones</label>
                    <textarea rows="8" class="input-xlarge"></textarea>
                </p>
                <p>
                    <button type="submit" class="btn btn-primary">Continuar</button>
                </p>              
                
            </form>
			
</asp:Content>