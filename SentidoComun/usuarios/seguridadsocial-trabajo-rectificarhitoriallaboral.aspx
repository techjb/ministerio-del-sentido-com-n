﻿<%@ Page Language="C#" MasterPageFile="Usuarios.Master"  AutoEventWireup="true" CodeBehind="seguridadsocial-trabajo-rectificarhitoriallaboral.aspx.cs" Inherits="SentidoComun.usuarios.seguridadsocial_trabajo_rectificarhitoriallaboral" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server" >
            <ul class="breadcrumb">
                <li><a href="seguridadsocial-trabajo.aspx">Trabajo</a> <span class="divider">/</span></li>
                <li class="active">Rectificar historial laboral</li>
            </ul>
			<div class="page-header">
				<h3>Rectificar historial laboral</h3>
			</div>
            <form class="well">
                <legend><asp:Label ID="LabelEmpresa" runat="server" Text=""></asp:Label></legend>
            
                <p>
                    <label>Fecha de comienzo.</label>
                    <input type="date" class="input-medium" placeholder="">

                    <label>Fecha de finalización.</label>
                    <input type="date" class="input-medium" placeholder="">
                    <label>Observaciones</label>
                    <textarea rows="8" class="input-xlarge"></textarea>
                </p>
                <p>
                    <button type="submit" class="btn btn-primary">Continuar</button>
                </p>              
                
            </form>
			
</asp:Content>