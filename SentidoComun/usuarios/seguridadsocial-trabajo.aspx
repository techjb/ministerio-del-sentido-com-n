﻿<%@ Page Language="C#" MasterPageFile="Usuarios.Master" AutoEventWireup="true" CodeBehind="seguridadsocial-trabajo.aspx.cs" Inherits="SentidoComun.usuarios.seguridadsocial_trabajo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server" >
            
           

			<div class="page-header">
				<h1>Trabajo</h1>
			</div>
           
            <p>
               Actualmente figuras como <span class="label label-success">trabajador activo</span> en régimen de <strong>Autónomo</strong> desde el <strong>28/09/2004</strong>.<br /> 
               <a href="seguridadsocial-trabajo-bajaautonomo.aspx">Darme de baja de autónomo</a>
            </p>
             
            <h3>Historial laboral</h3>
			
            
            <table class="table table-striped table-bordered table-condensed">
				<thead>
					<tr>
						<th>Régimen</th>
						<th>Empresa</th>
                        <th>Comienzo</th>
                        <th>Fin</th>
						<th>Días</th>
                        <th></th>
					</tr>
				</thead>
				<tbody>
                    <tr><td>Autónomo</td><td>Autónomo: Actividades de ingeniería</td><td>1/10/2009</td><td></td><td><span id="timeSpan3">1105</span></td><td><a type="button" class="btn btn-mini" href="seguridadsocial-trabajo-rectificarhitoriallaboral.aspx?empresa=Autónomo%3A+Actividades+de+ingeniería">Rectificar</a></td></tr>
                    <tr><td>General</td><td>Telecom Universe S.L.</td><td>1/09/2005</td><td>30/09/2009</td><td>1490</td><td><a type="button" class="btn btn-mini" href="seguridadsocial-trabajo-rectificarhitoriallaboral.aspx?empresa=Telecom+Universe+S.L.">Rectificar</a></td></tr>
                    <tr><td>General</td><td>Implemental Systems S.L.</td><td>28/09/2004</td><td>15/08/2005</td><td>322</td><td><a type="button" class="btn btn-mini" href="seguridadsocial-trabajo-rectificarhitoriallaboral.aspx?empresa=Implemental+Systems+S.L.">Rectificar</a></td></tr>
                </tbody>
			</table>
            <p>
                Has trabajado un total de <strong>2917 días</strong>.
            </p>
            
            
            <h3>Cotización</h3>
			<p>
               Base actual de cotización: <span class="label label-info">850,20 €</span>. <a href="seguridadsocial-trabajo-cambiarbasecotizacion.aspx">Cambiar la base de cotización</a><br />
                Has cotizado durante <strong>2934 días</strong> un total de <strong>18.756,72 €</strong>.   
            </p>
            <div id="placeholder" style="display:block;height:300px;margin:20px 0;width:99%;"></div>
             
            
            <%=script %>
            <script type="text/javascript">
                $(function () {
                    $.getScript("../js/jquery.flot.js", function () {
                        $.getScript("../js/jquery.flot.resize.js", function () {
                            LoadPlot();
                        });
                    });


                });
                function LoadPlot() {
                   
                    var data = [
		            
                    {
                        label: 'Cotización',
                        data: dataCotizacion
                    },
                    {
                        label: 'Base',
                        data: dataBase
                    },
                    {
                        label: 'Recargo',
                        data: dataRecargo
                    }
                    ];

                    var options = {
                        legend: {
                            show: true,
                            margin: 10,
                            backgroundOpacity: 0.5
                        },
                        points: {
                            show: true,
                            radius: 3
                        },
                        lines: {
                            show: true
                        },
                        grid: {
                            borderWidth: 1,
                            hoverable: true
                        },
                        xaxis: {
                            mode: "time" 
                            //axisLabel: 'Mes',
                            //ticks:dataMonths
                            //tickDecimals: 0
                            // mode: "time",
                            //min: (new Date(2005, 9, 1)).getTime(),
                            //max: (new Date()).getTime()
                        },
                        yaxis: {
                            tickSize: 100,
                            tickDecimals: 0
                        }
                    };

                    function showTooltip(x, y, contents) {
                        $('<div id="tooltip">' + contents + '</div>').css({
                            position: 'absolute',
                            display: 'none',
                            top: y + 5,
                            left: x + 5,
                            border: '1px solid #D6E9C6',
                            padding: '2px',
                            'background-color': '#DFF0D8',
                            opacity: 0.80
                        }).appendTo("body").fadeIn(200);
                    }
                    var previousPoint = null;
                    $("#placeholder").bind("plothover", function (event, pos, item) {
                        if (item) {
                            if (previousPoint != item.dataIndex) {
                                previousPoint = item.dataIndex;

                                $("#tooltip").remove();
                                showTooltip(item.pageX, item.pageY, item.series.label + ": " + item.datapoint[1] + " €");
                            }
                        }
                        else {
                            $("#tooltip").remove();
                            previousPoint = null;
                        }
                    });
                    $.plot($("#placeholder"), data, options);

                    for (var i = 0; i < dataPeriodo.length; i++) {
                        var periodo = dataPeriodo[i][0] + " " + dataPeriodo[i][1] ;
                        var base = dataBase[i][1] + " €";
                        var cotizacion = dataCotizacion[i][1]+ " €";
                        var recargo = dataRecargo[i][1]+ " €";
                        $('#tablaDatosCotizacion > tbody:last').append('<tr><td>' + periodo + '</td><td>'+base+'</td><td>'+  cotizacion +'</td><td>'+ recargo +'</td></tr>');
                    }
                }
            </script>
                        <p>
                <button type="button" class="btn" data-toggle="collapse" data-target="#divTablaDatosCotizacion">Mostrar la tabla de datos</button>
            </p>
            <div id="divTablaDatosCotizacion" class="collapse">
                <table id="tablaDatosCotizacion" class="table table-striped table-bordered table-condensed">
				    <thead>
					    <tr>
						    <th>Periodo</th>
						    <th>Base</th>
                            <th>Cuota</th>
                            <th>Regargo</th>
					    </tr>
				    </thead>
				    <tbody>
                    </tbody>
			    </table>
            </div>
            <p>
                ¿Algún error en la cotización? <a href="seguridadsocial-trabajo-rectificarhistorialcotizacion.aspx">Solicitar una rectificación</a>
            </p>
			
            
</asp:Content>