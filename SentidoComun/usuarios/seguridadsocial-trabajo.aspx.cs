﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Globalization;

namespace SentidoComun.usuarios
{
    public partial class seguridadsocial_trabajo : System.Web.UI.Page
    {
        public string script = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            ((Usuarios)Master).liTrabajo.Attributes.Add("class", "active");
            LoadData();
        }

        protected void LoadData()
        {
            StringBuilder dataPeriodo = new StringBuilder();
            StringBuilder dataBase = new StringBuilder();
            StringBuilder dataCotizacion = new StringBuilder();
            StringBuilder dataRecargo = new StringBuilder();            
            DateTime date = new DateTime(2005, 10, 1);

            int length = 84;

            for (int i = 0; i < length; i++)
            {
                double Base = i < 3 ? 770.40 : i < 15 ? 785.7 : i < 27 ? 801.3 : i < 39 ? 817.20 : i < 51 ? 833.40 : i < 63 ? 848.80 : 850.2;
                double cotizacion = i < 3 ? 153.12 : i < 15 ? 156.16 : i < 27 ? 159.25 : i < 39 ? 244.35 : i < 51 ? 249.18 : i < 63 ? 257.7 : 254.21;

                string timespan = (DateTimeToUnixTimestamp(date)*1000).ToString().Replace(",",".");
                dataBase.Append("[" + timespan + "," + Base.ToString().Replace(",",".") + "]");
                dataCotizacion.Append("[" + timespan + "," + cotizacion.ToString().Replace(",", ".") + "]");
                dataRecargo.Append("[" + timespan + ",0]");
                dataPeriodo.Append("[" + date.Year + ",'"+date.ToString("MMM", new CultureInfo( "es-ES", false ))+"']");

                if (i < length - 1)
                {
                    dataBase.Append(",");
                    dataCotizacion.Append(",");
                    dataRecargo.Append(",");
                    dataPeriodo.Append(",");
                }
                date = date.AddMonths(1);
            }
            
            StringBuilder cstext2 = new StringBuilder();
            cstext2.Append("<script type=\"text/javascript\">");
            cstext2.Append("var dataBase = [" + dataBase + "]; var dataCotizacion = [" + dataCotizacion + "]; var dataRecargo = [" + dataRecargo + "]; var dataPeriodo = [" + dataPeriodo + "];");
            cstext2.Append("</script>");
            //ClientScriptManager cs = Page.ClientScript;
            //cs.RegisterStartupScript(this.GetType(), "ScriptData", cstext2.ToString(), false);
            script = cstext2.ToString();
        }

        public double DateTimeToUnixTimestamp(DateTime dateTime)
        {
            return (dateTime - new DateTime(1970, 1, 1).ToLocalTime()).TotalSeconds;
        }
    }
}