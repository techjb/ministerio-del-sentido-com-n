﻿<%@ Page Language="C#" MasterPageFile="Usuarios.Master"  AutoEventWireup="true" CodeBehind="trafico-multas-identificar.aspx.cs" Inherits="SentidoComun.usuarios.trafico_multas_identificar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server" >
            <ul class="breadcrumb">
                <li><a href="trafico-multas.aspx">Multas y puntos</a> <span class="divider">/</span></li>
                <li class="active">Identificación de conductor</li>
            </ul>
			<div class="page-header">
				<h3>Identificación de conductor</h3>
			</div>
            <form class="well">
                <legend>Necesitamos que proporciones la siguiente información</legend>
                <p>
                    <label>Nombre de la persona que conducía el vehículo.</label>
                    <input type="text" class="input-large" placeholder="">

                    <label>Su DNI.</label>
                    <input type="text" class="input-small" placeholder="">
                   
                </p>
                <p>
                    <button type="submit" class="btn btn-primary">Continuar</button>
                </p>              
                
            </form>
			
</asp:Content>