﻿<%@ Page Language="C#" MasterPageFile="Usuarios.Master"   AutoEventWireup="true" CodeBehind="trafico-multas-pagar.aspx.cs" Inherits="SentidoComun.usuarios.trafico_multas_pagar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server" >
            <ul class="breadcrumb">
                <li><a href="trafico-multas.aspx">Multas y puntos</a> <span class="divider">/</span></li>
                <li class="active">Pago de multas</li>
            </ul>
			<div class="page-header">
				<h3>Pago de multa</h3>
			</div>
            <form class="well">
                <p>
                    <label>Importe a pagar</label>
                    <strong><asp:Label ID="LabelImporte" runat="server" Text=""></asp:Label></strong>
                    
                <p>
                    <label>Pagar con</label>
                    <select>
                        <option selected="selected">Cuenta principal</option>
                        <option>Cuenta secundaria</option>
                    </select>
                    <a href="micuenta-datosbancarios.aspx">Consultar mis datos bancarios</a>
                </p>
                   
                </p>
                <p>
                    <button type="submit" class="btn btn-primary">Continuar</button>
                </p>              
                
            </form>
			
</asp:Content>