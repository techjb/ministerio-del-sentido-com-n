﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SentidoComun.usuarios
{
    public partial class trafico_multas_pagar : System.Web.UI.Page
    {
        public string importe = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            ((Usuarios)Master).liMultas.Attributes.Add("class", "active");

            if (Request.Params["importe"] != null)
            {
                this.LabelImporte.Text = Request.Params["importe"].ToString() + " €";
            }
        }
    }
}