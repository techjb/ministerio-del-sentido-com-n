﻿<%@ Page Language="C#"  MasterPageFile="Usuarios.Master"  AutoEventWireup="true" CodeBehind="trafico-multas-recurrir.aspx.cs" Inherits="SentidoComun.usuarios.trafico_multas_recurrir" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server" >
            <ul class="breadcrumb">
                <li><a href="trafico-multas.aspx">Multas y puntos</a> <span class="divider">/</span></li>
                <li class="active">Recurrir multa</li>
            </ul>
			<div class="page-header">
				<h3>Recurrir la multa</h3>
			</div>
            <form class="well">
                <legend>Razones por las qué consideras que no debes pagar la multa</legend>
                <p>
                    <textarea class="input-xxlarge" rows="10" placeholder="Argumenta de forma razonable y detallada, aporta el máximo de información."></textarea>
                    <label>Adjunta las imágenes o archivos que consideres oportunos</label>
                    <input type="file" class="file" />
                   
                </p>
                <p>
                    <button type="submit" class="btn btn-primary">Continuar</button>
                </p>              
                
            </form>
			
</asp:Content>