﻿<%@ Page Language="C#" MasterPageFile="Usuarios.Master" AutoEventWireup="true" CodeBehind="trafico-multas.aspx.cs" Inherits="SentidoComun.usuarios.trafico_multas" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server" >

            <script type="text/javascript">
                $(function () {
                    var bottomAligned = { 'placement': 'bottom' };
                    $('#importe1').popover(bottomAligned);
                    $('#importe2').popover(bottomAligned);


                });
                </script>

          
			<div class="page-header">
				<h1>Multas y puntos de tráfico</h1>
			</div>
            <h3>Multas pendientes de pago</h3>
           

            <table class="table table-striped table-bordered table-condensed">
				<thead>
					<tr>
						<th>Fecha</th>
                        <th>Lugar</th>
						<th>Multado por</th>
						<th>Sanción</th>
                        <th>Puntos</th>
                        <th></th>
					</tr>
				</thead>
				<tbody>
                    <tr><td>07/10/2012 21:35</td><td ><a href="javascript:MostrarPopupMapa('Gran Vía 23, 28010, Madrid.','https://maps.google.es/maps?f=q&source=s_q&hl=es&geocode=&q=C%2F+Gran+V%C3%ADa+23,+28028,+Madrid&aq=&sll=40.380028,-3.669434&sspn=9.25215,21.643066&t=w&ie=UTF8&hq=&hnear=Calle+Gran+V%C3%ADa,+23,+28013+Madrid,+Comunidad+de+Madrid&ll=40.419995,-3.701881&spn=0.001129,0.002642&z=19&output=embed');">Gran Vía 23, Madrid.</a></td><td>Estacionamiento en zona prohibida</td><td >200 € <a href="#" rel="popover" id="importe1" data-content="20% de recargo despúes del 07/09/2012"><i class="icon-info-sign"></i></a></td><td >-2</td><td><div class="btn-group pull-right"><a class="btn btn-mini dropdown-toggle" data-toggle="dropdown" href="#">Acciones<span class="caret"></span></a><ul class="dropdown-menu"><li><a href="trafico-multas-pagar.aspx?importe=200">Pagarla</a></li><li><a href="trafico-multas-recurrir.aspx">Recurrirla</a></li></ul></div></td></tr>
                    <tr><td>29/08/2012 09:40</td><td ><a href="javascript:MostrarPopupMapa('Ctra N-VI Km 21','https://maps.google.es/maps?f=q&source=s_q&hl=es&geocode=&q=40.484361,-3.861574&aq=&sll=40.472917,-3.825794&sspn=0.009027,0.021136&t=w&ie=UTF8&z=18&ll=40.484361,-3.861574&output=embed');">Ctra N-VI Km 21</a></td><td >Exceso de velocidad</td><td >120 € <a href="#" rel="popover" id="importe2" data-content="100 € + recargo 20% = 120 €"><i class="icon-info-sign"></i></a></td><td ></td><td ><div class="btn-group pull-right"><a class="btn btn-mini dropdown-toggle" data-toggle="dropdown" href="#">Acciones<span class="caret"></span></a><ul class="dropdown-menu"><li><a href="trafico-multas-pagar.aspx?importe=120">Pagarla</a></li><li><a href="trafico-multas-recurrir.aspx">Recurrirla</a></li></ul></div></td></tr>
                    <tr><td colspan="5"></td><td><a href="trafico-multas-pagar.aspx?importe=320" class="btn btn-mini btn-primary pull-right">Pagar todas</a></td></tr>
                </tbody>
			</table>
          
            <p>
                <button type="button" class="btn" data-toggle="collapse" data-target="#historialDeMultas">Historial de multas</button>
            </p>
            <div id="historialDeMultas" class="collapse">
                <p>
                    En total has perdido 7 puntos y 1.100 €.
                </p>
                <table class="table table-striped table-bordered table-condensed">
				    <thead>
					    <tr>
						    <th>Fecha</th>
                            <th>Lugar</th>
						    <th>Multado por</th>
						    <th>Sanción</th>
                            <th>Puntos</th>
                            <th></th>
					    </tr>
				    </thead>
				    <tbody>
                        <tr><td >18/04/2011 04:12</td><td ><a href="javascript:MostrarPopupMapa('Ctra N-VI Km 18','https://maps.google.es/maps?f=q&amp;source=s_q&amp;hl=es&amp;geocode=&amp;q=40.513673,-3.882175&amp;aq=&amp;&amp;t=w&amp;ie=UTF8&amp;z=18&amp;output=embed');">Ctra N-VI Km 18</a></td><td >Exceso de velocidad</td><td >300 €</td><td >-2</td><td ><span class="label label-success">Pagada</span></td></tr>
                        <tr><td >15/12/2011 14:25</td><td ><a href="javascript:MostrarPopupMapa('C/ Princesa 90, 28015, Madrid','https://maps.google.es/maps?q=calle+de+princesa+90,+madrid&amp;hl=es&amp;sll=40.434627,-3.719068&amp;sspn=0.009032,0.021136&amp;t=w&amp;z=19&amp;output=embed');">C/ Princesa 90, 28015, Madrid</a></td><td >Saltarse señal de STOP</td><td >100 €</td><td ></td><td ><span class="label label-success">Pagada</span></td></tr>
                        <tr><td >25/10/2010 09:35</td><td ><a href="javascript:MostrarPopupMapa('Paseo de las Delicias, Madrid','https://maps.google.es/maps?f=q&amp;source=s_q&amp;hl=es&amp;geocode=&amp;q=calle+de+ferrocarril+2,+madrid&amp;aq=&amp;sll=40.400713,-3.694679&amp;sspn=0.00113,0.002642&amp;t=w&amp;ie=UTF8&amp;hq=&amp;ll=40.400977,-3.694351&amp;spn=0.00113,0.002642&amp;z=18&amp;output=embed');">Paseo de las Delicias, Madrid</a></td><td >Hablar por teléfono móvil</td><td >200 €</td><td >-3</td><td ><span class="label label-success">Pagada</span></td></tr>
                        <tr><td >04/02/2010 13:00</td><td ><a href="javascript:MostrarPopupMapa('Crta Robledo, 28200, El Escorial','https://maps.google.es/maps?q=Carretera+de+Robledo,+San+Lorenzo+de+El+Escorial&amp;hl=es&amp;sll=40.570744,-4.175107&amp;sspn=0.036054,0.117245&amp;t=w&amp;z=18&amp;output=embed');">Crta Robledo, 28200, El Escorial</a></td><td >Adelantamiento con línea contínua</td><td >200 €</td><td >-2</td><td ><span class="label label-success">Pagada</span></td></tr>
                        <tr><td >02/01/2010 22:10</td><td ><a href="javascript:MostrarPopupMapa('CL 505, Ávila','https://maps.google.es/?ie=UTF8&amp;ll=40.586044,-4.413908&amp;spn=0.018023,0.042272&amp;t=w&amp;z=15&amp;output=embed');">CL 505, Ávila</a></td><td >Conducción temeraria</td><td >150 €</td><td ></td><td ><span class="label label-info">Recurrida</span></td></tr>
                        <tr><td >03/09/2009 23:15</td><td ><a href="javascript:MostrarPopupMapa('Av. de España 44, 28230, Las Rozas de Madrid.','https://maps.google.es/maps?f=q&amp;source=s_q&amp;hl=es&amp;geocode=&amp;sll=40.487511,-3.875411&amp;sspn=0.009025,0.021136&amp;t=w&amp;ie=UTF8&amp;hq=&amp;z=18&amp;ll=40.487511,-3.875411&amp;output=embed');">Av. de España 44, 28230, Las Rozas de Madrid.</a></td><td >No llevar luces puestas</td><td >100 €</td><td ></td><td ><span class="label label-success">Pagada</span></td></tr>
                        <tr><td >16/04/2008 13:54</td><td ><a href="javascript:MostrarPopupMapa('A 62, Km 30','https://maps.google.es/?ie=UTF8&amp;ll=41.596564,-4.8224&amp;spn=0.002218,0.005284&amp;t=w&amp;z=18&amp;output=embed');">A 62, Km 30</a></td><td >Exceso de velocidad</td><td >200 €</td><td ></td><td ><span class="label label-success">Pagada</span></td></tr>
                    </tbody>
			    </table>
            </div>
            <h3>Requerimientos</h3>
           
            <table class="table table-striped table-bordered table-condensed">
				<thead>
					<tr>
						<th>Fecha</th>
                       <th>Lugar</th>
						<th>Se requiere</th>						
                        <th></th>
					</tr>
				</thead>
				<tbody>
                    <tr><td><span id="date03">04/08/2012</span> 12:45</td><td><a href="javascript:MostrarPopupMapa('Crta de Madrid Segovia, 4006, Segovia.','https://maps.google.es/maps?f=q&amp;source=s_q&amp;hl=es&amp;geocode=&amp;q=carretera+de+madrid,+segovia&amp;aq=&amp;sll=40.929791,-4.109488&amp;sspn=0.028046,0.066047&amp;ie=UTF8&amp;hq=&amp;hnear=Carretera+de+Madrid,+40006+Segovia,+Castilla+y+Le%C3%B3n&amp;ll=40.927589,-4.109419&amp;spn=0.014023,0.033023&amp;t=h&amp;z=16&amp;iwloc=A&amp;output=embed');">Crta de Madrid Segovia, 4006, Segovia.</a></td><td>Identificar al conductor</td><td><a href="trafico-multas-identificar.aspx" class="btn btn-mini btn-primary pull-right">Identificar</a></td></tr>
                </tbody>
			</table>
            <p>
                No ha habido otros requerimientos anteriormente.
            </p>
            <h3>Puntos</h3>
            <p class="lead">
               Actualmente tienes <span class="badge badge-success" style="font-size:24px;">8</span> puntos.
            </p>
            <p>
                <button type="button" class="btn" data-toggle="collapse" data-target="#historialDePuntos">Historial de puntos</button>
            </p>
            <div id="historialDePuntos" class="collapse">
                <table class="table table-striped table-bordered table-condensed">
				    <thead>
					    <tr>
						    <th>Fecha</th>
						    <th>Movimiento</th>
                            <th>Puntos</th>
                            <th>Saldo final</th>
						    <th>Organismo</th>
					    </tr>
				    </thead>
				    <tbody>
                        <tr><td>18/04/2011</td><td>Sanción de tráfico</td><td align="center"><span style="font-weight:bold;color:Red;">-2</span></td><td align="center">8</td><td>Jefatura Provincial De Madrid</td></tr>
                        <tr><td>25/10/2010</td><td>Sanción de tráfico</td><td align="center"><span style="font-weight:bold;color:Red;">-3</span></td><td align="center">10</td><td>Jefatura Provincial De Madrid</td></tr>
                        <tr><td>04/02/2010</td><td>Sanción de tráfico</td><td align="center"><span style="font-weight:bold;color:Red;">-2</span></td><td align="center">13</td><td>Jefatura Provincial De Madrid</td></tr>
                        <tr><td>04/02/2008</td><td>Bonificación</td><td align="center"><span style="font-weight:bold;color:Green;">+1</span></td><td align="center">15</td><td></td></tr>
                        <tr><td>04/02/2007</td><td>Bonificación</td><td align="center"><span style="font-weight:bold;color:Green;">+2</span></td><td align="center">14</td><td></td></tr>
                        <tr><td>01/05/2005</td><td>12 puntos iniciales</td><td align="center"><span style="font-weight:bold;color:Green;">12</span></td><td align="center">12</td><td></td></tr>
                    </tbody>
			    </table>
            </div>
</asp:Content>