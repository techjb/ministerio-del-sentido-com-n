﻿<%@ Page Language="C#" MasterPageFile="Usuarios.Master" AutoEventWireup="true" CodeBehind="trafico-permisos-mercanciaspeligrosas.aspx.cs" Inherits="SentidoComun.usuarios.trafico_permisos_mercanciaspeligrosas" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server" >
            
            <ul class="breadcrumb">
                <li><a href="trafico-permisos.aspx">Permisos de conducción</a> <span class="divider">/</span></li>
                <li class="active">Autorización para transporte mercancías peligrosas</li>
            </ul>
			<div class="page-header">
				<h3>Autorización para el transporte de mercancías peligrosas</h3>
			</div>
            <p>
                <div class="alert alert-info">
                    Para solicitar la autorización para transporte mercancías peligrosas debes haber realizado con aprovechamiento un curso en un centro autorizado.
                </div>
            </p>
            <form class="well">
              <p>
                <label>Nº de inscripción del centro de formación</label>
                <input  type="text" placeholder="" value="">
              
                <label>Prueba que se solicita</label>
                <label class="radio"><input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>Obtención</label>
                <label class="radio"><input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">Prórroga</label>
                <label class="radio"><input type="radio" name="optionsRadios" id="optionsRadios3" value="option3" >Básica común</label>
                <label class="radio"><input type="radio" name="optionsRadios" id="optionsRadios4" value="option4" >Cisternas</label>
                <label class="radio"><input type="radio" name="optionsRadios" id="optionsRadios5" value="option5">Clase 1</label>
                <label class="radio"><input type="radio" name="optionsRadios" id="optionsRadios6" value="option6">Clase 7</label>
                    
                
              
                <label>La autorización tiene un coste de <strong>26,80 €</strong> que se cargará en</label>
                <select>
                    <option selected="selected">Cuenta principal</option>
                    <option>Cuenta secundaria</option>
                </select>
                <a href="micuenta-datosbancarios.aspx">Consultar mis datos bancarios</a>
              </p>
              <br />
              <p>
                <button type="submit" class="btn btn-primary">Continuar</button>
              </p>
            </form>
			
</asp:Content>