﻿<%@ Page Language="C#" MasterPageFile="Usuarios.Master"  AutoEventWireup="true" CodeBehind="trafico-permisos-solicitarduplicado.aspx.cs" Inherits="SentidoComun.usuarios.trafico_permisos_solicitarduplicado" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server" >
            <ul class="breadcrumb">
                <li><a href="trafico-permisos.aspx">Permisos de conducción</a> <span class="divider">/</span></li>
                <li class="active">Solicitar duplicado</li>
            </ul>
			<div class="page-header">
				<h3>Duplicado del permiso de conducir</h3>
			</div>
            <form class="well">
              <legend>¿Donde deseas recibirlo?</legend>
              <p>
                <label>Dirección</label>
                <input  class="input-xlarge" type="text" placeholder="C/Castillo de Belmonte 8 - 3ºA" value="C/Castillo de Belmonte 8 - 3ºA">
              
                <label>Código postal</label>
                <input  class="input-mini" type="text" placeholder="28232" value="28232">
              </p>
              <p>
                <label>El duplicado del permiso de condución tiene un coste de <strong>19,40 €</strong> que se cargará en</label>
                <select>
                    <option selected="selected">Cuenta principal</option>
                    <option>Cuenta secundaria</option>
                </select>
                <a href="micuenta-datosbancarios.aspx">Consultar mis datos bancarios</a>
              </p>
              <p>
                <button type="submit" class="btn btn-primary">Continuar</button>
              </p>
            </form>
			
</asp:Content>