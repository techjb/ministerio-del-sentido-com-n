﻿<%@ Page Language="C#" MasterPageFile="Usuarios.Master"  AutoEventWireup="true" CodeBehind="trafico-permisos-solicitarpermiso.aspx.cs" Inherits="SentidoComun.usuarios.trafico_permisos_solicitarpermiso" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server" >
            
            <ul class="breadcrumb">
                <li><a href="trafico-permisos.aspx">Permisos de conducción</a> <span class="divider">/</span></li>
                <li class="active">Inscripción para nuevo permiso</li>
            </ul>
			<div class="page-header">
				<h3>Inscripción para obtención de nuevo permiso</h3>
			</div>
            <p>
                <div class="alert alert-info">
                    Para obtener un permiso de conducción es necesario ser declarado apto en las pruebas teóricas, de destreza o de circulación, que correspondan para cada caso.
                </div>
            </p>
            <form class="well">
              <p>
                <label>Nº de inscripción del centro de formación</label>
                <input  type="text" placeholder="" value="">
              
                <label>Permiso que se solicita</label>
                <label class="radio"><input type="radio" name="optionsRadios" id="optionsRadios1" value="option1">BTP</label>
                <label class="radio"><input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">B + E</label>
                <label class="radio"><input type="radio" name="optionsRadios" id="optionsRadios3" value="option3" >C1 + E</label>
                <label class="radio"><input type="radio" name="optionsRadios" id="optionsRadios4" value="option4" >C</label>
                <label class="radio"><input type="radio" name="optionsRadios" id="optionsRadios5" value="option5">C + E</label>
                <label class="radio"><input type="radio" name="optionsRadios" id="optionsRadios6" value="option6">D1</label>
                <label class="radio"><input type="radio" name="optionsRadios" id="optionsRadios7" value="option7">D1 + E</label>
                <label class="radio"><input type="radio" name="optionsRadios" id="optionsRadios8" value="option8">D</label>
                <label class="radio"><input type="radio" name="optionsRadios" id="optionsRadios9" value="option9">D + E</label>
                    
                
              
                <label>La autorización tiene un coste de <strong>87,60 €</strong> que se cargará en</label>
                <select>
                    <option selected="selected">Cuenta principal</option>
                    <option>Cuenta secundaria</option>
                </select>
                <a href="micuenta-datosbancarios.aspx">Consultar mis datos bancarios</a>
              </p>
              <p>
                <button type="submit" class="btn btn-primary">Continuar</button>
              </p>
            </form>
			
</asp:Content>