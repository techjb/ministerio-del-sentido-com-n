﻿<%@ Page Language="C#" MasterPageFile="Usuarios.Master" AutoEventWireup="true" CodeBehind="trafico-permisos.aspx.cs" Inherits="SentidoComun.usuarios.trafico_permisos" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server" >
            <script type="text/javascript">
                $(function () {
                    $('#obs1').tooltip();
                    $('#obs2').tooltip();
                    $('#obs3').tooltip();
                    $('#obs4').tooltip();
                    
                });
            </script>
            
           
			<div class="page-header">
				<h1>Permisos de conducción</h1>
			</div>
           
            <table class="table table-striped table-bordered">
				<thead>
					<tr>
						<th></th>
                        <th>Válido desde</th>
						<th>Válido hasta</th>
						<th>Observaciones</th>
					</tr>
				</thead>
				<tbody>
                    <tr><td>A1<img alt="moto" src=" ../img/motorcycle.png" style="vertical-align:middle;margin-left:8px;"></td><td>16/07/2005</td><td>16/07/2025</td><td><span id="obs1" rel="tooltip" href="#" data-original-title="Necesitas lentes para conducir"><i class="icon-eye-open"></i></span></td></tr>  
                    <tr><td>A2<img alt="moto" src="../img/motorbike.png" style="vertical-align:middle;margin-left:8px;"></td><td>16/07/2005</td><td>16/07/2025</td><td><span id="obs2" rel="tooltip" href="#" data-original-title="Necesitas lentes para conducir"><i class="icon-eye-open"></i></span></td></tr>  
                    <tr><td>A<img alt="moto" src="../img/motorbike.png" style="vertical-align:middle;margin-left:16px;"></td><td>16/07/2005</td><td>16/07/2025</td><td><span id="obs3" rel="tooltip" href="#" data-original-title="Necesitas lentes para conducir"><i class="icon-eye-open"></i></span></td></tr>  
                    <tr><td>B<img alt="coche" src="../img/car.png" style="vertical-align:middle;margin-left:16px;"></td><td>1/09/2007</td><td>1/09/2027</td><td><span id="obs4" rel="tooltip" href="#" data-original-title="Necesitas lentes para conducir"><i class="icon-eye-open"></i></span></td></tr>
                </tbody>
			</table>
            <p class="lead">
                Último test psicotécnico realizado el <strong>17/10/2011</strong>.
            </p>
            <p>
                ¿Has perdido tu permiso? <a href="trafico-permisos-solicitarduplicado.aspx">Pedir un duplicado</a>.<br />
                ¿Deseas llevar mercancías peligrosas? <a href="trafico-permisos-mercanciaspeligrosas.aspx">Obtener autorización</a>.<br />
                ¿Quieres obtener un nuevo permiso de conducción? <a href="trafico-permisos-solicitarpermiso.aspx">Solicitar un nuevo permiso</a>.
            </p>
            
			

</asp:Content>