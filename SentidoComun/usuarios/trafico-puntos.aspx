﻿<%@ Page Language="C#" MasterPageFile="Usuarios.Master" AutoEventWireup="true" CodeBehind="trafico-puntos.aspx.cs" Inherits="SentidoComun.usuarios.trafico_puntos" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server" >
            
            <ul class="breadcrumb">
                <li><a href="index.aspx">Inicio</a> <span class="divider">/</span></li>
                <li class="active">Puntos de tráfico</li>
            </ul>

			<div class="page-header">
				<h1>Puntos de tráfico</h1>
			</div>
            <p class="lead">
                Actualmente tienes <span class="badge badge-success" style="font-size:24px;">8</span> puntos.
            </p>
            <h3>Historial de puntos</h3>
            <p>
               
            </p>
             <table class="table table-striped table-bordered table-condensed">
				<thead>
					<tr>
						<th>Fecha</th>
						<th>Movimiento</th>
                        <th>Puntos</th>
                        <th>Saldo final</th>
						<th>Organismo</th>
					</tr>
				</thead>
				<tbody>
                    <tr><td>18/04/2011</td><td>Sanción de tráfico</td><td align="center"><span style="font-weight:bold;color:Red;">-2</span></td><td align="center">8</td><td>Jefatura Provincial De Madrid</td></tr>
                    <tr><td>25/10/2010</td><td>Sanción de tráfico</td><td align="center"><span style="font-weight:bold;color:Red;">-3</span></td><td align="center">10</td><td>Jefatura Provincial De Madrid</td></tr>
                    <tr><td>04/02/2010</td><td>Sanción de tráfico</td><td align="center"><span style="font-weight:bold;color:Red;">-2</span></td><td align="center">13</td><td>Jefatura Provincial De Madrid</td></tr>
                    <tr><td>04/02/2008</td><td>Bonificación</td><td align="center"><span style="font-weight:bold;color:Green;">+1</span></td><td align="center">15</td><td></td></tr>
                    <tr><td>04/02/2007</td><td>Bonificación</td><td align="center"><span style="font-weight:bold;color:Green;">+2</span></td><td align="center">14</td><td></td></tr>
                    <tr><td>01/05/2005</td><td>12 puntos iniciales</td><td align="center"><span style="font-weight:bold;color:Green;">12</span></td><td align="center">12</td><td></td></tr>
                </tbody>
			</table>
            
			

</asp:Content>