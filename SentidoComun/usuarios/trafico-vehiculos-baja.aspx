﻿<%@ Page Language="C#" MasterPageFile="Usuarios.Master"  AutoEventWireup="true" CodeBehind="trafico-vehiculos-baja.aspx.cs" Inherits="SentidoComun.usuarios.trafico_vehiculos_baja" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server" >
            <ul class="breadcrumb">
                <li><a href="trafico-vehiculos.aspx">Vehículos</a> <span class="divider">/</span></li>
                <li class="active">Baja de vehículo</li>
            </ul>
			<div class="page-header">
				<h3>Dar de baja el vehículo</h3>
			</div>
                    <ul id="myTab" class="nav nav-tabs">
                      <li class="active"><a href="#temporal" data-toggle="tab">Baja temporal</a></li>
                      <li class=""><a href="#definitiva" data-toggle="tab">Baja definitiva</a></li>
                    </ul>
	                <div id="myTabContent" class="tab-content">
                        
            
                          <div class="tab-pane fade active in" id="temporal">
                            
                            <p>
                                <div class="alert alert-info">
                                    Después de dar de baja el vehículo de forma temporal, no podrás circular con él, y podrás volverlo a dar de alta en cualquier momento.
                                </div>
                            </p>
                            <form class="well">
                            <p>
                                <label>Motivo de la baja</label>
                                <label class="radio">
                                    <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
                                    Voluntaria
                                </label>
                                <label class="radio">
                                    <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
                                    Sustracción
                                </label>
                                <label class="radio">
                                    <input type="radio" name="optionsRadios" id="optionsRadios3" value="option3">
                                    Finalización del contrato de arrendamiento.
                                </label>
                            </p>
                            <p>
                                Declaro bajo mi responsabilidad que el vehículo ha sido retirado de circulación, por lo que solicito la baja indicada.
                            </p>
                            <p>
                                <button type="submit" class="btn btn-danger">Continuar</button>
                            </p>
                            </form>
                          </div>
                        
                      <div class="tab-pane fade" id="definitiva">
                        <p>
                            <div class="alert alert-info">
                                Para realizar la baja definitiva, deberás llevarlo a un Centro Autorizado de Tratamiento y firmar la solicitud de baja. El centro debe entregarte el certificado de destrucción del vehículo, y  el justificante de baja definitivo que te servirá para acreditar ante cualquier Administración que tu vehículo ha causado baja en Tráfico.
                            </div>
                        </p>
                       
                      </div>
                    </div>
              
              
            
			
</asp:Content>