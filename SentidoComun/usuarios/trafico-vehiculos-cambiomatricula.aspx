﻿<%@ Page Language="C#" MasterPageFile="Usuarios.Master" AutoEventWireup="true" CodeBehind="trafico-vehiculos-cambiomatricula.aspx.cs" Inherits="SentidoComun.usuarios.trafico_vehiculos_cambiomatricula" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server" >
            <ul class="breadcrumb">
                <li><a href="trafico-vehiculos.aspx">Vehículos</a> <span class="divider">/</span></li>
                <li class="active">Cambio de matrícula</li>
            </ul>
			<div class="page-header">
				<h3>Cambio de matrícula</h3>
			</div>


            <form class="well">
                <p>
                    <label>Selecciona el motivo del cambio</label>
                    <label class="radio"><input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>Transmisión</label>
                    <label class="radio"><input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">Cambio de domicilio</label>
                    <label class="radio"><input type="radio" name="optionsRadios" id="optionsRadios3" value="option3">Matriculación en el extranjero con matrícula previa española.</label>
                    <label class="radio"><input type="radio" name="optionsRadios" id="optionsRadios4" value="option4">Razones de seguridad.</label>
                </p>
                <p>
                    <label>El cambio de matrícula tiene un coste de  <strong>91,80 €</strong> que se cargará en</label>
                    <select>
                        <option selected="selected">Cuenta principal</option>
                        <option>Cuenta secundaria</option>
                    </select>
                    <a href="micuenta-datosbancarios.aspx">Consultar mis datos bancarios</a>
                </p>
                <p>
                    <button type="submit" class="btn btn-primary">Continuar</button>
                </p>
              
            </form>
			
</asp:Content>