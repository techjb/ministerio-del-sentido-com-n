﻿<%@ Page Language="C#" MasterPageFile="Usuarios.Master" AutoEventWireup="true" CodeBehind="trafico-vehiculos-transferir.aspx.cs" Inherits="SentidoComun.usuarios.trafico_vehiculos_transferir" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server" >
            <ul class="breadcrumb">
                <li><a href="trafico-vehiculos.aspx">Vehículos</a> <span class="divider">/</span></li>
                <li class="active">Transferencia de vehículo</li>
            </ul>
			<div class="page-header">
				<h3>Transferencia de vehículo</h3>
			</div>
            <p>
                    <div class="alert alert-block">
                        <h4>Importante</h4>
                        Nunca entregues el vehículo sin tener en tus manos el contrato de compraventa o una fotocopia del mismo  (si es entre particulares) o factura (si lo entregas a un compraventa) y una fotocopia del documento de identidad en España del comprador (DNI/Tarjeta de Residencia, si son personas físicas, o CIF, si son empresas). Si es un compraventa, deberá constar el CIF en la factura. Es importante indicar en el contrato la hora y minutos en que entregas el vehículo: recuerda que las multas de los radares fijos y por no haber pasado la ITV te seguirán llegando en tanto no se produzca en el Registro de Vehículos el cambio de titularidad.
                    </div>
                </p>
            <form class="well">
                
                <p>
                    <label>NIF de la persona a la que lo transfieres</label>
                    <input type="text" class="input-small" />
                    <label>Modo de transmisión</label>
                    <select>
                        <option>Venta</option>
                        <option>Donación</option>
                        <option>Herencia</option>
                        <option>Subasta</option>
                        <option>Otros</option>
                    </select>
                    <label>Servicio al que se destina</label>
                    <select>
                        <option>-- Particular --</option>
                        <option selected="selected">Sin especificar</option>
                        <option>Actividad económica</option>
                        <option>Agrícola</option>
                        <option>Recreativo</option>
                        <option>Obras</option>
                        <option>Vehículo de ferias</option>
                        <option>Vivienda</option>
                        <option>-- Público --</option>
                        <option>Sin especificar</option>
                        <option>Taxi</option>
                        <option>Mercancías peligrosas</option>
                        <option>Bomberos</option>
                        <option>Mercancías perecederas</option>
                        <option>Alquiler sin conductor</option>
                        <option>Alquiler con conductor</option>
                        <option>Auxilio en carretera</option>
                        <option>Basurero</option>
                        <option>Protec. Civil y salvamento</option>
                        <option>Ambulancia</option>
                        <option>Transporte escolar</option>
                        <option>Ministerio defensa</option>
                        <option>Aprendizaje conducción</option>
                        <option>Funerario</option>
                        <option>Policía</option>
                        <option>Actividad económica</option>
                    </select>
                </p>
                <p>
                    <button type="submit" class="btn btn-primary">Continuar</button>
                </p>              
                
            </form>
			
</asp:Content>