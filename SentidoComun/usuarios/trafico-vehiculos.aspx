﻿<%@ Page Language="C#" MasterPageFile="Usuarios.Master" AutoEventWireup="true" CodeBehind="trafico-vehiculos.aspx.cs" Inherits="SentidoComun.usuarios.trafico_vehiculos" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server" >
           
			<div class="page-header">
				<h1>Vehículos</h1>
			</div>
            <h3>Inscritos a tu nombre</h3>
            <p>
               
            </p>

              <script type="text/javascript">
                  $(function () {
                      var bottomAligned = { 'placement': 'bottom' };
                      $('#matricula1').popover();
                      $('#matricula2').popover();
                      $('#itv1').popover(bottomAligned);
                      $('#itv2').popover(bottomAligned);

                      $('#aseguradora1').popover(bottomAligned);
                      $('#aseguradora2').popover(bottomAligned);
                      

                  });
                </script>
            <table class="table table-striped table-bordered table-condensed">
				<thead>
					<tr>
						<th>Matrícula</th>
                        <th>Modelo</th>
						<th>Itv</th>
						<th>Seguro</th>
                        <th></th>
						
					</tr>
				</thead>
				<tbody>
                    <tr>
                        <td>3686 GBC <a href="#" rel="popover" data-original-title="3686 GBC" data-content="<p><strong>Primera matriculación:</strong> 6/5/1999</p><p><strong>Segunda matriculación:</strong> 7/8/2005</p><br /><p><div class='alert alert-error'>Para pedir un cambio de matrícula tienes que pasar primero la ITV.</div></p></a>" id="matricula1"><i class="icon-info-sign"></i></a></td>
                        <td>
                            <a href="#"  data-toggle="collapse" data-target="#vehiculo2">FIAT Seicento</a>
                            <div id="vehiculo2" class="collapse">
                                <p>
                                    <dl class='dl-horizontal'>
                                        <dt>Nº de bastidor</dt><dd>WBHGT77222JU82172</dd>
                                        <dt>Uso</dt><dd>Particular</dd>
                                        <dt>Combustible</dt><dd>Gasolina</dd>
                                        <dt>Cilindrada</dt><dd>1995 cc</dd>
                                        <dt>Potencia</dt><dd>54 CV</dd>
                                        <dt>Máxima carga</dt><dd>1430 Kg</dd>
                                        <dt>Máximo de plazas</dt><dd>5</dd>
                                    </dl>
                                </p>        
                            </div>
                        </td>
                        <td><span class="label label-important">Caducada</span> <a href="#" rel="popover" id="itv1" data-content="<p><strong>Última revisión:</strong> 01/09/2008</p><p><strong>Caducó el día:</strong> 01/09/2011</p><br /><p><a class='btn btn-primary' href='javascript:MostrarCentrosItv();'><i class='icon-search icon-white'></i>  Centros de ITV cercanos</a></p>"><i class="icon-info-sign"></i></a></td>
                        <td><span class="label label-success">Vigente</span> <a href="#" rel="popover" id="aseguradora1" data-content="<p><strong>Aseguradora:</strong><br />MMT Seguros (Nif G28010817)</p><p><strong>Póliza nº:</strong><br />245564</p><p><strong>Perido de validez:</strong><br />19/06/2012 - 19/06/2013</p>"><i class="icon-info-sign"></i></a></td>
                        <td><div class="btn-group pull-right"><a class="btn btn-mini dropdown-toggle" data-toggle="dropdown" href="#">Acciones<span class="caret"></span></a><ul class="dropdown-menu"><li><a href="trafico-vehiculos-baja.aspx">Darlo de baja</a></li></ul></div></td>
                    </tr>
                    <tr>
                        <td>8240 HFH <a href="#" rel="popover" data-original-title="8240 HFH" data-content="<p><strong>Primera matriculación:</strong> 28/04/2010</p><br /><p><a class='btn' href='trafico-vehiculos-cambiomatricula.aspx'>Cambiar la matrícula</p></a>" id="matricula2"><i class="icon-info-sign"></i></a></td>
                        <td>
                            <a href="#"  data-toggle="collapse" data-target="#vehiculo1">Piaggio X Evo 125</a>
                            <div id="vehiculo1" class="collapse">
                                <p>
                                    <dl class='dl-horizontal'>
                                        <dt>Nº de bastidor</dt><dd>WBCCT55684KU213456</dd>
                                        <dt>Uso</dt><dd>Particular</dd>
                                        <dt>Combustible</dt><dd>Gasolina</dd>
                                        <dt>Cilindrada</dt><dd>124 cc</dd>
                                        <dt>Potencia</dt><dd>110 KW</dd>
                                        <dt>Máxima carga</dt><dd>800 Kg</dd>
                                        <dt>Máximo de plazas</dt><dd>2</dd>
                                    </dl>
                                </p>        
                            </div>
                        </td>
                        <td><span class="label label-success">Revisada</span> <a href="#" rel="popover" id="itv2" data-content="<p><strong>Última revisión:</strong> 01/09/2011</p><p><strong>Válida hasta:</strong> 01/09/2013</p>"><i class="icon-info-sign"></i></a></td>
                        <td><span class="label label-important">Caducado</span> <a href="#" rel="popover" id="aseguradora2" data-content="<p><strong>Aseguradora:</strong><br />Mapfre (Nif A/28141935)</p><p><strong>Póliza nº:</strong><br />6687446213</p><p><strong>Perido de validez:</strong><br />4/4/2010 - 4/4/2011</p><br /><p><div class='alert alert-error'><strong>Contacta con tu compañía o contrata un seguro nuevo</strong></div></p>"><i class="icon-info-sign"></i></a></td>
                        <td><div class="btn-group pull-right"><a class="btn btn-mini dropdown-toggle" data-toggle="dropdown" href="#">Acciones<span class="caret"></span></a><ul class="dropdown-menu"><li><a href="trafico-vehiculos-transferir.aspx">Transferirlo a otra persona</a></li><li><a href="trafico-vehiculos-baja.aspx">Darlo de baja</a></li></ul></div></td>
                    </tr>
                </tbody>
			</table>
            <p>
                <a class="btn" href="javascript:EsSoloUnaIdea();"><i class=" icon-plus"></i> Dar de alta un nuevo vehículo</a>
            </p>
            <h3>Transferidos a otros</h3>
            <p>
                No existen vehículos transferidos a otros.
            </p>
			

</asp:Content>